from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from freshdesk_api.views import *

urlpatterns = patterns('',
    url(r'api/', include([
        url(r'^contacts/(?P<method>\w{1,50})/$', process_request),
        url(r'^tickets/(?P<method>\w{1,50})/$', process_request),
        url(r'^edit/$', process_request),
        url(r'^discuss/$', process_request),
        url(r'^permissions/$', process_request),
    ])),
)