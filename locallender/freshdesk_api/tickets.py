from django.template import Context,loader
from django.http import HttpResponse, StreamingHttpResponse
from django.conf import settings
from pprint import pprint
import json
import pycurl
from StringIO import StringIO
            
   
def test(debug_type, debug_msg):
    print "shintu(%d): %s" % (debug_type, debug_msg)
    

 # Create ticket acccount 
def create(content):
    try:
        storage = StringIO()
        scope = ("helpdesk/tickets.json")
        postData1 = { "helpdesk_ticket": { "description": content['des'], "subject": content['sub'], "email": content['email'], "priority": 1, "status": 2 }, "cc_emails": content['cc_email'] }
        postData=str(postData1).replace("'",'"')
        url = settings.FRESHDESK_URL+scope
        print url
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "Q5pmkjJUmw6on3UORP:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(c.WRITEDATA, storage)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        c.setopt(pycurl.POSTFIELDS, postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
        
 #update user data
def update(content):
    try:
        storage = StringIO()
        scope = ("helpdesk/tickets")
        postData1 = { "helpdesk_ticket": { "priority":content['priority'], "status":content['status'] }}
        postData=str(postData1).replace("'",'"')
        module = __name__.split('.')
        url = settings.FRESHDESK_URL+scope+"/"+content['id']+".json"
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "Q5pmkjJUmw6on3UORP:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(pycurl.CUSTOMREQUEST, "PUT")
        c.setopt(c.WRITEDATA, storage) 
        c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        c.setopt(pycurl.POSTFIELDS, postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
    
# View all user data          
def viewall(content):
    try:
        storage = StringIO()
        scope = ("helpdesk/tickets")
        module = __name__.split('.')
        url = settings.FRESHDESK_URL
        url = settings.FRESHDESK_URL+scope+".json"
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "Q5pmkjJUmw6on3UORP:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(c.WRITEDATA, storage)
        #c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.FOLLOWLOCATION, 1)
       # c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        #c.setopt(pycurl.POSTFIELDS,postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
#view ticket     
def view(content):
    try:
        storage = StringIO()
        scope = ("helpdesk/tickets")
        postData = '{ "user": { "name":"Manisha Sharma", "email":"mss.manishasharma@gmail.com" }}'
        url = settings.FRESHDESK_URL+scope+"/"+content['id']+".json"
        print url
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "Q5pmkjJUmw6on3UORP:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(c.WRITEDATA, storage)
        #c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.FOLLOWLOCATION, 1)
       # c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        #c.setopt(pycurl.POSTFIELDS,postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
 #ticket pick     
def ticketpick(content):
    try:
        storage = StringIO()
        scope = ("helpdesk/tickets")
        postData = '{ "user": { "name":"Manisha Sharma", "email":"mss.manishasharma@gmail.com" }}'
        url = settings.FRESHDESK_URL+scope+"/"+content['id']+"/pick_tickets.json"
        print url
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "Q5pmkjJUmw6on3UORP:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(c.WRITEDATA, storage)
        #c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.FOLLOWLOCATION, 1)
       # c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        #c.setopt(pycurl.POSTFIELDS,postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
#DELETE pick ticket   
def delete(content):
    try:
        storage = StringIO()
        scope = ("helpdesk/tickets")
        url = settings.FRESHDESK_URL+scope+"/"+content['id']+"/pick_tickets.json"
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "Q5pmkjJUmw6on3UORP:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(c.WRITEDATA, storage) 
        c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(pycurl.FOLLOWLOCATION, 1)
        c.setopt(pycurl.CUSTOMREQUEST, "DELETE")
        #c.setopt(c.NOPROGRESS, 0)
        #c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        #c.setopt(pycurl.POSTFIELDS, postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
        
 #Restore pick Ticket
def restore(content):
    try:
        storage = StringIO()
        scope = ("helpdesk/tickets")
        module = __name__.split('.')
        url = settings.FRESHDESK_URL+scope+"/"+content['id']+"/restore.json"
        print url
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "Q5pmkjJUmw6on3UORP:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(pycurl.CUSTOMREQUEST, "PUT")
        c.setopt(c.WRITEDATA, storage) 
        c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        #c.setopt(pycurl.POSTFIELDS, postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
   
# Assign A Ticket  to other user user_id    
def assign(content):
    try:
        storage = StringIO()
        scope = ("helpdesk/tickets")
        module = __name__.split('.')
        url = settings.FRESHDESK_URL+scope+"/"+content['id']+"/assign.json?responder_id="+content['user_id']
        print 'shintu'
        print url
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "Q5pmkjJUmw6on3UORP:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(pycurl.CUSTOMREQUEST, "PUT")
        c.setopt(c.WRITEDATA, storage) 
        c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        #c.setopt(pycurl.POSTFIELDS, postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
        
        
        
        