from django.template import Context,loader
from django.http import HttpResponse, StreamingHttpResponse
from django.core.context_processors import csrf
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.middleware.csrf import get_token

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import authenticate

from django.contrib.auth.models import User    
from django.views.generic.base import TemplateView
import json

from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.db import *
import re
from django.core.mail import send_mail, BadHeaderError
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
import httplib2
import requests
http = httplib2.Http()
from django.utils.encoding import iri_to_uri
from django.utils.http import urlquote
import hashlib
import hmac
import time
from pprint import pprint # to remove later
from freshdesk_api import contacts, tickets
import ast

def process_request(request, method, **args):
    path = request.path
    content = str(request.GET["data"]).encode('utf8')
    content = content.replace('%25253a', ':')
    content = content.replace('%25252b', ' ')
    content = content.replace('%252b', ' ')
    content = content.replace('%253a', ':')
    content = content.replace('%3a', ':')
    content = content.replace('%2b', ' ')
    response = {}    
    try:
        if request.user.is_authenticated():
            # the password verified for the user
            if request.user.is_active:
                slashparts = path.split('/')
                basename = '/'.join(slashparts[:3]) + '/'
                dirname = '/'.join(slashparts[:-1]) + '/'
                content = ast.literal_eval(str(content).encode('utf8'))
                if slashparts[3]=='contacts':
                # call functions of the modules as per dynamic methods from request
                 status_code ,data = getattr(contacts, method)(content, **args)
                elif slashparts[3]=='tickets':
                  status_code ,data = getattr(tickets, method)(content, **args)
                result = {'status':status_code,'data':data}
                return HttpResponse(json.dumps(result), content_type='application/json')
            else:
                data = {'error': 'You do not have permission to access these end points !'}
                return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            # the authentication system was unable to verify the username and password
            print("The username and password were incorrect.")
            #return StreamingHttpResponse('staff login result')
            data = {'error': 'user login session not found !'}
            return HttpResponse(json.dumps(data), content_type='application/json')
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
        
        
        
        
        
        