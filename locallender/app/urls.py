from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView

from app.views import *
from django.contrib.auth.views import login,logout
from django.contrib.auth.decorators import user_passes_test

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
login_forbidden =  user_passes_test(lambda u: u.is_anonymous(), '/dashboard')

class SimpleStaticView(TemplateView):
    def get_template_names(self):
        return [self.kwargs.get('template_name') + ".html"]
    
    def get(self, request, *args, **kwargs):

        from django.contrib.auth import authenticate, login

        #from django.contrib.auth import authenticate, login

        #if request.user.is_anonymous():
            # Auto-login the User for Demonstration Purposes
            #user = authenticate()
            #login(request, user)
        return super(SimpleStaticView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super(SimpleStaticView, self).get(request, *args, **kwargs)


urlpatterns = patterns('',
    url(r'^api/', include('app.api.urls')),
    url(r'^dashboard/api/', include('app.api.urls')),
    url(r'^customer-dashboard/api/', include('app.api.urls')),
    url(r'^lender-dashboard/api/', include('app.api.urls')),
    url(r'^myaccount/api/', include('app.api.urls')),
    url(r'^mylender/api/', include('app.api.urls')),
    url(r'^leads/api/', include('app.api.urls')),
    url(r'^staffdashboard/api/', include('app.api.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^(?P<template_name>\w+)$', SimpleStaticView.as_view(), name='app'),
    url(r'^$', TemplateView.as_view(template_name='base.html'), name="homepage"),
    url(r'^accounts/register/$', login_forbidden(TemplateView.as_view(template_name='lenders/SignUp.html'))),


    url(r'^lenderSignUp/', TemplateView.as_view(template_name='lenderSignUp.html')),
    url(r'^SignIn/', TemplateView.as_view(template_name='SignIn.html')),

    #url(r'^accounts/register', TemplateView.as_view(template_name='lenderSignUp.html')),
    url(r'^accounts/login/$', login_forbidden(login), name="login"),
    #url(r'^login/', TemplateView.as_view(template_name='login.html')),

    url(r'^forgotPassword/', TemplateView.as_view(template_name='forgotpassword.html')),
    #url(r'^application/', TemplateView.as_view(template_name='static.html')),
    
    #dashboard url's
    url(r'^dashboard/', TemplateView.as_view(template_name='dashboard.html')),
    url(r'^customer-dashboard/', TemplateView.as_view(template_name='customers/dashboard.html')),
    url(r'^lender-dashboard/$', TemplateView.as_view(template_name='lenders/profile.html')),
    url(r'^lender-dashboard/get-started$', TemplateView.as_view(template_name='lenders/dashboard.html')),
    url(r'^staffdashboard/(?P<template_name>\w+)$', SimpleStaticView.as_view(), name='staff'),
    
    
    url(r'^mylender/', TemplateView.as_view(template_name='customers/myLender.html')),
    url(r'^myaccount/', TemplateView.as_view(template_name='customers/profile.html')),
    url(r'^leads/', TemplateView.as_view(template_name='lenders/leads.html')),
    
    url(r'^login/sso/', include('freshdesk.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    # django-registration URLs:
    url(r'^accounts/', include('registration.backends.default.urls')),
    
    url(r'^lender-dashboard/payments/', include('djstripe.urls', namespace="djstripe")),
    url(r'^freshdesk/', include('freshdesk_api.urls')),
    
    #url(r'^notifications/settings/$', include('notifications.urls')),  
    url(r'^avatar/', include('avatar.urls')),
    url(r'staffdashboard/helpdesk/', include('helpdesk.urls')),
)

if settings.DEBUG:
    from django.views.static import serve
    urlpatterns += patterns('',
        url(r'^(?P<path>favicon\..*)$', serve, {'document_root': settings.STATIC_ROOT}),
        url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], serve, {'document_root': settings.MEDIA_ROOT}),
        url(r'^%s(?P<path>.*)$' % settings.STATIC_URL[1:], 'django.contrib.staticfiles.views.serve', dict(insecure=True)),
    )
