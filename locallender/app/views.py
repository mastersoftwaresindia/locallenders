from django.template import Context,loader
from django.http import HttpResponse
from django.core.context_processors import csrf
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.middleware.csrf import get_token

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render

from django.contrib.auth.models import User
    
from django.views.generic.base import TemplateView

from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.db import *
import re
from django.core.mail import send_mail, BadHeaderError
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context


class HiyyaBaseClass(TemplateView):
    """Hiyya project base class"""
    
    template_name = "base.html"
    params = { 'static_url':settings.STATIC_URL, 'static_media':settings.MEDIA_URL, 'static_templates':settings.TEMPLATE_DIRS }

    def get(self, request, *args, **kwargs):
        context = ({ 'static': self.params })# compute what you want to pass to the template
        return self.render_to_response(context)
    
class ServicesClass(TemplateView):
    """Hiyya project base class"""
    
    template_name = "static.html"
    params = { 'static_url':settings.STATIC_URL, 'static_media':settings.MEDIA_URL, 'static_templates':settings.TEMPLATE_DIRS }

    def post(self, request, *args, **kwargs):
        context = ({ 'static': self.params })# compute what you want to pass to the template
        return self.render_to_response(context)
    
class PortfolioClass(TemplateView):
    """Hiyya project base class"""
    
    template_name = "folio.html.twig"
    params = { 'static_url':settings.STATIC_URL, 'static_media':settings.MEDIA_URL, 'static_templates':settings.TEMPLATE_DIRS }

    def get(self, request, *args, **kwargs):
        context = ({ 'static': self.params })# compute what you want to pass to the template
        return self.render_to_response(context)

class BlogClass(TemplateView):
    """Hiyya project base class"""
    
    template_name = "blog.html.twig"
    params = { 'static_url':settings.STATIC_URL, 'static_media':settings.MEDIA_URL, 'static_templates':settings.TEMPLATE_DIRS }

    def get(self, request, *args, **kwargs):
        context = ({ 'static': self.params })# compute what you want to pass to the template
        return self.render_to_response(context)
    
class ContactClass(TemplateView):
    """Hiyya project base class"""
    
    template_name = "contact-us.html.twig"
    params = { 'static_url':settings.STATIC_URL, 'static_media':settings.MEDIA_URL, 'static_templates':settings.TEMPLATE_DIRS }

    def get(self, request, *args, **kwargs):
        context = ({ 'static': self.params })# compute what you want to pass to the template
        return self.render_to_response(context)
    
class ShootersClass(TemplateView):
    """Hiyya project base class"""
    
    template_name = "shooters.html.twig"
    params = { 'static_url':settings.STATIC_URL, 'static_media':settings.MEDIA_URL, 'static_templates':settings.TEMPLATE_DIRS }

    def get(self, request, *args, **kwargs):
        context = ({ 'static': self.params })# compute what you want to pass to the template
        return self.render_to_response(context)
    
class CreateClass(TemplateView):
    """Hiyya project base class"""
    
    template_name = "create.html.twig"
    params = { 'static_url':settings.STATIC_URL, 'static_media':settings.MEDIA_URL, 'static_templates':settings.TEMPLATE_DIRS }

    def get(self, request, *args, **kwargs):
        context = ({ 'static': self.params })# compute what you want to pass to the template
        return self.render_to_response(context)
    
    def post(self, request):
        """Get post parameters for registration"""
        username, email, password, cpassword  = request.POST['username'], request.POST['email'], request.POST['password'], request.POST['cpassword']
        try:
            validate_email(email)    
            if not re.match(r'^[a-zA-Z0-9]*$', username):
                return HttpResponse("Username must be alphanumeric!")
            
            if not password==cpassword:
                return HttpResponse("Password do not match !")
            
            user = User.objects.create_user(username, email, password)
            if user is not None:
                user.is_active = False # save newly registred user account status as inactive.
                user.save()
                
                html = get_template('res/account-activation.html.twig')
                d = Context({ 'username': user }) 

                subject, from_email, to = 'Account activation', 'jeevanj12@yahoo.com', email
                text_content = html.render(d)
                html_content = html.render(d)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                if msg.send():
                    return HttpResponse("User registered successfully, check your email to activate account.")
        except ValidationError as e: # if email or username is not valid
            return HttpResponse(e)
        except IntegrityError as e: # check duplicate user in database
            return HttpResponse("User " + username + " already exists.")
        
class LoginClass(TemplateView):
    """Hiyya project base class"""
    
    template_name = "sign-in.html.twig"
    params = { 'static_url':settings.STATIC_URL, 'static_media':settings.MEDIA_URL, 'static_templates':settings.TEMPLATE_DIRS }

    def get(self, request, *args, **kwargs):
        context = ({ 'static': self.params })# compute what you want to pass to the template
        return self.render_to_response(context)
    
    def post(self, request):
        """Get post parameters for registration"""
        username, email, password, cpassword  = request.POST['username'], request.POST['email'], request.POST['password'], request.POST['cpassword']
        try:
            validate_email(email)    
            if not re.match(r'^[a-zA-Z0-9]*$', username):
                return HttpResponse("Username must be alphanumeric!")
            
            if not password==cpassword:
                return HttpResponse("Password do not match !")
            
            user = User.objects.create_user(username, email, password)
            if user is not None:
                user.is_active = False
                user.save()
                email = EmailMessage('Hello', 'World', to=['youremail@somewhere.com'])
                if email.send():
                    return HttpResponse("User registered successfully, check your email to activate account.")
        except ValidationError as e: # if email or username is not valid
            return HttpResponse(e)
        except IntegrityError as e: # check duplicate user in database
            return HttpResponse("User " + username + " already exists.")