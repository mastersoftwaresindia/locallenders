from django.conf.urls import patterns, url, include

from .api import *

from .api import PostList, PostDetail, UserPostList
from .api import PhotoList, PhotoDetail, PostPhotoList

user_urls = patterns('',
    url(r'^/(?P<username>[0-9a-zA-Z_-]+)/posts$', UserPostList.as_view(), name='userpost-list'),
    url(r'^/(?P<username>[0-9a-zA-Z_-]+)$', UserDetail.as_view(), name='user-detail'),
    url(r'^$', UserList.as_view(), name='user-list')
)
post_urls = patterns('',
    url(r'^/(?P<pk>\d+)/photos$', PostPhotoList.as_view(), name='postphoto-list'),
    url(r'^/(?P<pk>\d+)$', PostDetail.as_view(), name='post-detail'),
    url(r'^$', PostList.as_view(), name='post-list')
)
photo_urls = patterns('',
    url(r'^/(?P<pk>\d+)$', PhotoDetail.as_view(), name='photo-detail'),
    url(r'^$', PhotoList.as_view(), name='photo-list')
)
staff_urls = patterns('',
    url(r'^/(?P<pk>\d+)$', PhotoDetail.as_view(), name='photo-detail'),
    url(r'^$', StaffLogin.as_view(), name='staff-login')
)
customer_urls = patterns('',
    url(r'^/(?P<pk>\d+)$', PhotoDetail.as_view(), name='photo-detail'),
    url(r'^$', CustomerSignUp.as_view(), name='customer-signup')
)
lender_urls = patterns('',
    url(r'^/(?P<pk>\d+)$', PhotoDetail.as_view(), name='photo-detail'),
    url(r'^$', GetLenders.as_view(), name='get-lenders')
)
urlpatterns = patterns('',
    url(r'^users', include(user_urls)),
    url(r'^posts', include(post_urls)),
    url(r'^photos', include(photo_urls)),
    url(r'^staff', include(staff_urls)),
    url(r'^customer', include(customer_urls)),
    url(r'^lendersignup$',LenderSignUp.as_view(), name='lender-signup'),
    url(r'^login',Login.as_view(), name='login'),
    url(r'^forgotpassword',ForgotPassword.as_view(), name='forgot-password'),
    url(r'^signout/$', SignOut.as_view(), name='signout'),
    url(r'^getLenders', include(lender_urls)),
    url(r'^approveLender/$', ApproveLender.as_view(), name='approve-lender'),
    url(r'^getActiveLenders/$', GetActiveLenders.as_view(), name='get-activelender'),
    url(r'^getCustomerInfo$', GetCustomerInfo.as_view(), name='get-customerinfo'),
    url(r'^getCustomers/$', GetCustomers.as_view(), name='get-customers'),
    url(r'^customerexists$',CustomerExistence.as_view(), name='customer-exists'),
    url(r'^mylender',MyLender.as_view(), name='my-lender'),
    url(r'^approveLead/$', ApproveLead.as_view(), name='approve-lead'),
    url(r'^saveLeadInfo$', SaveLeadInfo.as_view(), name='save-leadinfo'),
    url(r'^getMyLeads/$', GetMyLeads.as_view(), name='get-myleads'),
    url(r'^leadStatus/$', ChangeLeadStatus.as_view(), name='change-leadstatus'),
    url(r'^saveLenderInfo/$', SaveLenderInfo.as_view(), name='save-lenderinfo'),
    url(r'^getLenderInfo/$', GetLenderInfo.as_view(), name='get-lenderinfo'),
    #url(r'^lenderRequest/$', LenderRequest.as_view(), name='lender-request'),
  
)