import json
from .models import *
from rest_framework import generics, permissions
from django.http import HttpResponse, StreamingHttpResponse
from django.views.generic.base import View
from django.core.validators import validate_email
from .serializers import UserSerializer, PostSerializer, PhotoSerializer
from .models import User, Post, Photo
from .permissions import PostAuthorCanEditPermission
from django.views.generic.base import TemplateView
from django.conf import settings
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.conf import settings
from django.utils.encoding import iri_to_uri
from django.utils.http import urlquote
from django.core.exceptions import ImproperlyConfigured
from django.contrib.auth import authenticate
import hashlib
import hmac
import time
from django.core.mail import send_mail
from django.contrib.auth import login, logout
from django.views.generic.base import RedirectView

from django.core.urlresolvers import reverse

import sys
from geopy.geocoders import Nominatim
from geopy.geocoders import GoogleV3
from geopy.distance import great_circle

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.utils import timezone
import datetime
#from hellosign_sdk import HSClient
import stripe



'''code to use hello sign api
client = HSClient(api_key='0babcb384682960ab3074f907b46633f9335a569b7da1406965f0801ef321cd2')
signatureId = client.send_signature_request_embedded(
            test_mode=True,
            client_id='07cb906f6a297e6d9b1c6d2731fe76b0',
            subject="My First embedded signature request",
            message="Awesome, right?",
            signers=[{ 'email_address': 'mss.shintusingh@gmail.com', 'name': 'Sachin Sharma' }],
            files=['/home/mss/Downloads/My_First_embedded_signature_request.pdf']
)
print type(signatureId)
SIGNATURE_ID = signatureId
print "SIGNATURE_ID",SIGNATURE_ID

#obj = client.get_embedded_object('SIGNATURE_ID')
#sign_url = obj.sign_url
#print "url",sign_url'''

'''Stripe payment'''
'''class TestStripe(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
            print "inside stripe"
            data = request.POST
            print "data",data
            stripe.api_key = "sk_test_d1pjj1Eim1yflWqFSW2avsLb"
            # Get the credit card details submitted by the form
            token = request.POST.get('stripeToken')
            # Create the charge on Stripe's servers - this will charge the user's card
            try:
                charge = stripe.Charge.create(
                amount=1000, # amount in cents, again
                currency="usd",
                card=token,
                description="payinguser@example.com"
                )
            except stripe.CardError, e:
                # The card has been declined
                pass'''


         
'''Get Specific Leads for a Lender'''
class GetMyLeads(View):
    def get(self, request):
        try:
            dictList = []
            current_user = request.user
            userId=current_user.id
            leads = Leads.objects.filter(offered_to = userId)
            leadsLength = len(leads)
            if leads:
                for i in range(0,leadsLength):
                    for lead in leads:
                        keys = ['first_name','last_name','street_addr1','street_addr2','city','state','phone_home',
                            'Email','id','Zip','created_on']
                        
                        Leadinfo = [leads[i].first_name,leads[i].last_name,leads[i].street_addr1,leads[i].street_addr2,
                                    leads[i].city,leads[i].state,leads[i].phone_home,leads[i].Email,leads[i].id,
                                    leads[i].Zip,str(leads[i].created_on)]
                    dictList.append(dict(zip(keys, Leadinfo)))
                data = {'result': 'success','info' :dictList}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {'result': 'error','detail':'No Leads has been assigned'}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
        

'''Change Lead status'''
class ChangeLeadStatus(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data",data
            lead_status = data['lead_status']
            lead_id = data['lead_id']
            status_code = StatusCodes.objects.filter(name = lead_status)
            for status in status_code:
               code = status.value
            currentdate = datetime.datetime.now()
            userExist = LeadStatus.objects.filter(lead_id = lead_id )
            if userExist:
                LeadStatus.objects.filter(lead_id=lead_id).update(
                status_id = code,updated_on = currentdate)
            else:
                leadstatus=LeadStatus.objects.create(lead_id = lead_id,status_id = code,added_on = currentdate,updated_on = currentdate)
                leadstatus.save()
            data = {'result': 'success','detail':'Status changed'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
'''Get lender information for lender profile page '''
class GetLenderInfo(View):
    def get(self, request):
        try:
            current_user = request.user
            userId=current_user.id
            keys = ['username','email','phone_number','storeName','storeAddress','id','zip_code',
                    'ownerName','ownerPhone','ownerEmail','managerName','managerPhone','managerEmail','EinNumber',
                    'loanLimit']
            dictList = []
            Customerinfo = [current_user.username,current_user.email,current_user.phone_number,
            current_user.storeName,current_user.storeAddress,current_user.id,current_user.zip_code,
            current_user.ownerName,current_user.ownerPhone,current_user.ownerEmail,current_user.managerName,
            current_user.managerPhone,current_user.managerEmail,current_user.EinNumber,current_user.loanLimit]
            dictList.append(dict(zip(keys, Customerinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

'''Save Lender  info from lender profile page'''
class SaveLenderInfo(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data here",data
            current_user = request.user
            userId=current_user.id
            print "userId",userId
            User.objects.filter(id=userId).update(
            managerPhone=data['managerPhone'],loanLimit=data['loanLimit'],ownerEmail=data['ownerEmail'],
            EinNumber=data['EinNumber'],managerEmail=data['managerEmail'],ownerName=data['ownerName'],
            ownerPhone=data['ownerPhone'],managerName=data['managerName'],phone_number=data['phone_number'],
            storeAddress=data['storeAddress'])
            data = {'result': 'success','details':'Lender information saved successfully'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
        

'''Lender's request to staff member'''
'''class LenderRequest(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
            data = json.loads(request.body)
            print "data in lenders request",data'''
            

'''Lead approval by staffmember'''
class ApproveLead(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            customers = data['customers']
            flag = data['flag']
            length = len(customers)
            print 'length',length,data
            if flag=='staff':
                for i in range(0,length):
                    first_name = customers[i]['first_name']
                    last_name = customers[i]['last_name']
                    Email = customers[i]['Email']
                    phone_cell = customers[i]['Phone']
                    user_id = customers[i]['id']
                    source = flag
                    offered = '0'
                    lead = Leads.objects.filter(Email=Email)
                    exists = len(lead)
                    if exists == 0:
                        lead=Leads.objects.create(offered=offered,user_id=user_id,first_name=first_name,last_name=last_name,Email=Email,phone_cell=phone_cell,source=source)
                        lead.save()
                    else:
                        Leads.objects.filter(Email=Email).update(
                        offered=offered,
                        user_id=user_id,
                        first_name=first_name,
                        last_name=last_name,
                        Email=Email,
                        phone_cell=phone_cell,
                        source=source)
                    User.objects.filter(email=Email).update(
                        lead_conversion=1
                    )
                data = {'result': 'success'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                pass
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
            
            

'''Lead info saved by Lead'''
class SaveLeadInfo(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data",data
            fields=['email_alternate','middle_init','street_addr2','phone_cell','phone_work_ext']
            for field in fields:
                print "field",field
                if field not in data or data[field]==u'':
                    data[field] = None
            current_user = request.user
            currenttime=datetime.datetime.now()
            userId=current_user.id
            user_exists = Leads.objects.filter(user_id=userId)
            if user_exists:
                Leads.objects.filter(user_id=userId).update(
                first_name=data['first_name'],last_name=data['last_name'],middle_init=data['middle_init'],
                street_addr1=data['street_addr1'],street_addr2=data['street_addr2'],city=data['city'],
                state=data['state'],Zip=data['Zip'],social_security=data['social_security'],
                phone_home=data['phone_home'],phone_cell=data['phone_cell'],phone_work=data['phone_work'],
                phone_work_ext=data['phone_work_ext'],Email=data['Email'],birth_date=data['birth_date'],
                employer_name=data['employer_name'],pay_frequency=data['pay_frequency'],
                direct_deposit=data['direct_deposite'],pay_day1=data['pay_day1'],pay_day2=data['pay_day2'],
                bank_aba=data['bank_aba'],bank_account=data['bank_account'],bank_name=data['bank_name'],
                income_monthly=data['income_monthly'],own_home=data['own_home'],drivers_license=data['drivers_license'],
                drivers_license_st=data['drivers_license_st'],client_url_root=data['client_url_root'],
                client_ip_address=data['client_ip_address'],email_alternate=data['email_alternate'],
                months_employed=data['months_employed'],income_type=data['income_type'],
                is_military=data['is_military'],bank_account_type=data['bank_account_type'],
                requested_amount=data['requested_amount'],months_at_address=data['months_at_address'],months_at_bank=data['months_at_bank'],
                updated_on = currenttime)
                data = {'result': 'success','detail':'Successfully Updated'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                Leads.objects.create(
                first_name=data['first_name'],last_name=data['last_name'],middle_init=data['middle_init'],
                street_addr1=data['street_addr1'],street_addr2=data['street_addr2'],city=data['city'],
                state=data['state'],Zip=data['Zip'],social_security=data['social_security'],
                phone_home=data['phone_home'],phone_cell=data['phone_cell'],phone_work=data['phone_work'],
                phone_work_ext=data['phone_work_ext'],Email=data['Email'],birth_date=data['birth_date'],
                employer_name=data['employer_name'],pay_frequency=data['pay_frequency'],
                direct_deposit=data['direct_deposite'],pay_day1=data['pay_day1'],pay_day2=data['pay_day2'],
                bank_aba=data['bank_aba'],bank_account=data['bank_account'],bank_name=data['bank_name'],
                income_monthly=data['income_monthly'],own_home=data['own_home'],drivers_license=data['drivers_license'],
                drivers_license_st=data['drivers_license_st'],client_url_root=data['client_url_root'],
                client_ip_address=data['client_ip_address'],email_alternate=data['email_alternate'],
                months_employed=data['months_employed'],income_type=data['income_type'],
                is_military=data['is_military'],bank_account_type=data['bank_account_type'],
                requested_amount=data['requested_amount'],months_at_address=data['months_at_address'],
                months_at_bank=data['months_at_bank'],user_id=current_user.id,created_on = currenttime
                )
                User.objects.filter(id=userId).update(
                        lead_conversion=1
                    )
                data = {'result': 'success','detail':'Successfully Saved'}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
    
            

'''lender selection by customer'''
class MyLender(View):
    def get(self, request):
        try:
            current_user = request.user
            userId=current_user.id
            user = Leads.objects.filter(user_id = userId)
            if user:
                for i in user:
                    lender_id = i.offered_to
                    if lender_id:
                        lender = User.objects.filter(id = lender_id)
                        for user in lender:
                            keys = ['ownerName','first_name','last_name','Email','Phone','id','storeName','storeAddress',]
                            dictList = []
                            Lenderinfo = [user.ownerName,user.first_name,user.last_name,user.email,user.phone_number,user.id,
                                user.storeName,user.storeAddress]
                            dictList.append(dict(zip(keys, Lenderinfo)))
                        return HttpResponse(json.dumps(dictList), content_type='application/json')
                    else:
                        data = {'result': 'error','detail':'No Lender has been choosen Yet'}
                        return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {'result': 'error','detail':'No Lender has been choosen Yet'}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
    
    def post(self, request):
        try:
            current_user = request.user
            userId=current_user.id
            is_lead=current_user.lead_conversion
            data = json.loads(request.body)
            lender_id = data['lenderid']
            if is_lead:
                print "is_lead"
                Leads.objects.filter(user_id=userId).update(
                        offered_to=lender_id
                )
                data = {'result': 'success','detail':'Lender Choosen Successfully'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                print "no lead"
                data = {'result': 'error','detail':'Please fill your profile information on your account page to choose a lender'}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')



'''Get customers information for customer profile page '''
class GetCustomerInfo(View):
    def get(self, request):
        try:
            current_user = request.user
            userId=current_user.id
            userName=current_user.username
            email=current_user.email
            phoneNumber=current_user.phone_number
            first_name = current_user.first_name
            last_name = current_user.last_name
            zipCode=current_user.zip_code
            #print "data",userId,userName,phoneNumber,email
            keys = ['Name', 'Email','Phone','id','first_name','last_name','zipCode']
            dictList = []
            Customerinfo = [userName,email,phoneNumber,userId,first_name,last_name,zipCode]
            dictList.append(dict(zip(keys, Customerinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
    

'''check if customer already exists or not'''
class CustomerExistence(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data",data
            email = data['userEmail']
            if email:
                user = User.objects.filter(email=email).count()
                print "user",user
                if user:
                    data = {'result': 'error','detail':'User already exists'}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                else:
                    data = {'result': 'success','detail':'No user found'}
                    return HttpResponse(json.dumps(data), content_type='application/json') 
            else:
                data = {'result': 'error','detail':'Enter a valid email'}
                return HttpResponse(json.dumps(data), content_type='application/json')
                
        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

'''SignIn function for lenders as well customers'''
class Login(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            username,password=data['username'],data['password']
            user = User.objects.get(username=username)
            #check if user is not none and is active
            if user is not None:
                if user.is_active:
                    #check for user provided password 
                    userpassword = user.password
                    if password == userpassword:
                        #if the user is a customer
                        if user.group == 'Customers':
                            data = {'result': 'success','detail':'Welcome,Customer...You are logged in'}
                            return HttpResponse(json.dumps(data), content_type='application/json')
                        #if user is a lender
                        elif user.group == 'Lenders':
                            data = {'result': 'success','detail':'Welcome,Lender...You are logged in'}
                            return HttpResponse(json.dumps(data), content_type='application/json')
                    else:
                        data = {'result': 'error','detail':'Credentials did not  matched'}
                        return HttpResponse(json.dumps(data), content_type='application/json')
                else:
                    data = {'result': 'error','detail':'Your account is not active yet'}
                    return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')


'''Lender SignUp Class'''
class LenderSignUp(View):
    def get(self, request):
        # <view logic>user
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        #SignUp function for Lenders
        #Try part
        try:
            data = json.loads(request.body)
            zipCode = data['zipcode']
            #Get Long,Lat from lenders zipcode
            geolocator = GoogleV3()
            location = geolocator.geocode(zipCode)
            if location:
                longitude = location.longitude
                latitude = location.latitude
                email = data['email']
                user = User.objects.filter(email=email).count()
                if user:
                    data = {'result': 'error','details':'This email is already registered with us'}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                else:
                    #Get user data from request
                    username,email,password,entityname,state,name,phone_number,storeName,storeAddress,group,zipCode,longitude,latitude,city,service=data['name'],data['email'],data['password'],data['entityname'],data['state'],data['name'],data['phoneNumber'],data['storeName'],data['storeAddress'],data['group'],data['zipcode'],longitude,latitude,data['city'],data['service']
                    #Pass values to User Model
                    user=User.objects.create(entityname=entityname,password=password,email=email,
                    storeName=storeName,storeAddress=storeAddress,group=group,state=state,phone_number=phone_number,zip_code=zipCode,
                    longitude=longitude,latitude=latitude,is_active=0,city=city,service=service,username=username )
                    #Save Lender's information to database
                    user.save()
                    #Return response
                    data = {'result': 'success'}
                    return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {'result': 'error','details':"Please enter a valid zip code"}
                return HttpResponse(json.dumps(data), content_type='application/json')
        #Exception Part
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

        

'''Forgot Password class'''
class ForgotPassword(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            #get user email from request
            email = data['email']
            #check for user existence
            user = User.objects.get(email=email)
            if user is not None:
                password = user.password

                #send password to user's email address if exists

                send_mail('Password Reset Mail', 'Hello,This is a mail in response to your password forgotten request.Your Password is: %s' %password, 'LocalLendersStaff@example.com',
                [email], fail_silently=False)
                data = {'result': 'success',"detail":'Your password has been sent to your email address'}
                return HttpResponse(json.dumps(data), content_type='application/json')



            #if user is none
            else:
                data = {'result': 'error',"detail":'No User Found'}
                return HttpResponse(json.dumps(data), content_type='application/json')
        #if exception occurs
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')



'''Customer SignUp Class'''
class CustomerSignUp(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        data = json.loads(request.body)
        try:
            email,username,first_name,last_name,phoneNumber,zipCode,group = data['customerEmail'],data['first_name'],data['first_name'],data['last_name'],data['phone_number'],data['zip_code'],data['group']
            zipCode = data['zip_code']
            #Get Long,Lat from lenders zipcode
            geolocator = GoogleV3()
            location = geolocator.geocode(zipCode)
            if location:
                longitude = location.longitude
                latitude = location.latitude
                #save customer information to database
                try:
                    user=User.objects.create(first_name=first_name,username=username,last_name=last_name,email=email,group=group, zip_code=zipCode, phone_number=phoneNumber,longitude=longitude,latitude=latitude,password=phoneNumber)
                    user.save()
                    #sending mail for account registration confirmation
                    '''send_mail('Account registered confirmation', 'Hello,This is an account registeration email confirmation.Your account has been registered with LocalLenders.net.Please click below to login: http://localhost:9604/accounts/login. Your Password is: %s' %phoneNumber, 'LocalLendersStaff@example.com',
                    [email], fail_silently=False)'''
                    plaintext = get_template('registration/activation_email.txt')
                    htmly     = get_template('email-templates/account-reactivated.htm')
                    d = Context({ 'username': username,'phoneNumber':phoneNumber,'email':email})
                    subject, from_email, to = 'hello', 'from@example.com', email
                    text_content = plaintext.render(d)
                    html_content = htmly.render(d)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    data = {'result': 'success',"details":' An account activation email has been sent, check your emails for login and account password information.'}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                except Exception,e:
                    data = {'result': 'error','details':"Username already exists"}
                    return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {'result': 'error','details':"Please enter a valid zip code"}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')



'''Get All Lenders for staff dashboard'''
class GetLenders(View):
    def get(self, request):
        try:
            current_user = request.user
            customer_longitude,customer_latitude=current_user.longitude,current_user.latitude
            customer_location = (current_user.longitude,current_user.latitude)
            group = 'Lenders'
            lenders = User.objects.filter(group=group)
            keys = ['Name', 'Email', 'Store','Phone','Address','distance','id','is_active']
            dictList = []
            for lender in lenders:
                lender_location =(lender.longitude,lender.latitude)
                cust_loc = customer_location
                lend_loc = lender_location
                distance = great_circle(cust_loc, lend_loc).miles
                lendersinfo = [lender.ownerName,lender.email,lender.storeName,lender.phone_number,lender.storeAddress,distance,lender.id,lender.is_active]
                dictList.append(dict(zip(keys, lendersinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
'''Get all customers for staff dashboard '''
class GetCustomers(View):
    def get(self, request):
        try:
            current_user = request.user
            group = 'Customers'
            Customers = User.objects.filter(group=group,is_active=True)
            keys = ['Name', 'Email','Phone','id','days','seconds','lastlogindays','lastloginseconds','lastloginminutes',
            'lastloginhours','first_name','last_name','lead_conversion']
            dictList = []
            for customer in Customers:
                currentdate = timezone.now()
                #Get last login details
                lastlogin=customer.last_login
                logindifference=currentdate-lastlogin
                lastlogindays = logindifference.days
                lastloginseconds=int(logindifference.total_seconds())
                lastloginminutes=int(lastloginseconds/60)
                lastloginhours=int(lastloginseconds/3600)
                #Get joined date details
                datejoined=customer.date_joined
                difference = currentdate-datejoined
                daysdifference = difference.days
                secondsdifference=int(difference.total_seconds())
                customerinfo = [customer.first_name,customer.email,customer.phone_number,customer.id,daysdifference,secondsdifference,
                lastlogindays,lastloginseconds,lastloginminutes,lastloginhours,customer.first_name,customer.last_name,
                customer.lead_conversion]
                dictList.append(dict(zip(keys, customerinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        

'''Get Active Lenders for Customer dashboard'''
class GetActiveLenders(View):
    def get(self, request):
        try:
            current_user = request.user
            customer_longitude,customer_latitude=current_user.longitude,current_user.latitude
            customer_location = (current_user.longitude,current_user.latitude)
            group = 'Lenders'
            lenders = User.objects.filter(group=group,is_active=True)
            keys = ['Name', 'Email', 'Store','Phone','Address','distance','id','is_active','is_choosen']
            dictList = []
            data = Leads.objects.filter(user_id=request.user.id)
            if data:
                for d in data:
                    lender_id = d.offered_to
            else:
                lender_id = ''
            for lender in lenders:
                if lender_id == lender.id:
                    choosen = 1
                else:
                    choosen = 0
                lender_location =(lender.longitude,lender.latitude)
                cust_loc = customer_location
                lend_loc = lender_location
                distance = great_circle(cust_loc, lend_loc).miles
                lendersinfo = [lender.ownerName,lender.email,lender.storeName,lender.phone_number,lender.storeAddress,distance,lender.id,lender.is_active,choosen]
                dictList.append(dict(zip(keys, lendersinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
            #else:
                #data = {'result': 'error','details':"Please enter you full details on your profile page"}
                #return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
            
class StaffLogin(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            email, username, password = data['staffEmail'], data['staffUsername'], data['staffPassword']
            user = authenticate(username=username, password=password)
            if user is not None:
                # the password verified for the user
                if user.is_active and user.is_staff:
                    first_name = user.first_name
                    last_name = user.last_name
                    utctime = int(time.time())
                    data = '{0} {1}{2}{3}'.format(
                        first_name, last_name, email, utctime)
                    generated_hash = hmac.new(
                        settings.FRESHDESK_SECRET_KEY.encode(), data.encode(), hashlib.md5).hexdigest()
                    url = '{0}login/sso?name={1}&email={2}&timestamp={3}&hash={4}'.format(settings.FRESHDESK_URL,
                        urlquote('{0} {1}'.format(first_name, last_name)), urlquote(email), utctime, generated_hash)
                    res = HttpResponse(iri_to_uri(url))
                    return res
                else:
                    print("The password is valid, but the account has been disabled!")
            else:
                # the authentication system was unable to verify the username and password
                print("The username and password were incorrect.")
            #return StreamingHttpResponse('staff login result')
            data = {'error': 'user is none'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        

'''Approve a Lender'''        
class ApproveLender(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data",data
            lender_id = data['lenderId']
            lender_email = data['lenderEmail']
            lender = User.objects.filter(id=lender_id)
            if lender:
                for lenders in lender:
                    if lenders.is_active==False:
                        lenders.is_active = 1
                        lenders.save()
                        #sending mail to lender for the approval of account
                        send_mail('Account Approval confirmation', 'Hello,This is an account approval email confirmation.Your account has been registered with LocalLenders.net', 'LocalLendersStaff@example.com',
                        [lender_email], fail_silently=False)
                        
                    else:
                        lenders.is_active = 0
                        lenders.save()
                data = {'result': 'success','Details':'Lender state changed successfully'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {'error': 'no lender found'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            
        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
class SignOut(RedirectView):
    permanent = False
    query_string = True
 
    def get_redirect_url(self):
        print "here sachin"
        logout(self.request)
        return reverse('home')

class SignOut(RedirectView):
    permanent = False
    query_string = True
 
    def get_redirect_url(self):
        print "here sachin"
        logout(self.request)
        return reverse('home')


class UserList(generics.ListAPIView):
    model = User
    serializer_class = UserSerializer



class UserDetail(generics.RetrieveAPIView):
    model = User
    serializer_class = UserSerializer
    lookup_field = 'username'


class PostMixin(object):
    model = Post
    serializer_class = PostSerializer
    permission_classes = [
        PostAuthorCanEditPermission
    ]
    
    def pre_save(self, obj):
        """Force author to the current user on save"""
        obj.author = self.request.user
        return super(PostMixin, self).pre_save(obj)



class PostList(View):
    def get(self, request):
        # <view logic>
        return HttpResponse('result')
    
    def post(self, request):
        # <view logic>
        return HttpResponse('result')



class PostDetail(PostMixin, generics.RetrieveUpdateDestroyAPIView):
    pass


class UserPostList(generics.ListAPIView):
    model = Post
    serializer_class = PostSerializer
    
    def get_queryset(self):
        queryset = super(UserPostList, self).get_queryset()
        return queryset.filter(author__username=self.kwargs.get('username'))


class PhotoList(generics.ListCreateAPIView):
    model = Photo
    serializer_class = PhotoSerializer
    permission_classes = [
        permissions.AllowAny
    ]


class PhotoDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Photo
    serializer_class = PhotoSerializer
    permission_classes = [
        permissions.AllowAny
    ]


class PostPhotoList(generics.ListAPIView):
    model = Photo
    serializer_class = PhotoSerializer
    
    def get_queryset(self):
        queryset = super(PostPhotoList, self).get_queryset()
        return queryset.filter(post__pk=self.kwargs.get('pk'))
