# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table(u'api_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('phone_number', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('zip_code', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('group', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('entityname', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('ownerName', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('ownerEmail', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('ownerPhone', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('storeName', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('storeAddress', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('EinNumber', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('managerName', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('managerEmail', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('managerPhone', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('longitude', self.gf('django.db.models.fields.FloatField')()),
            ('latitude', self.gf('django.db.models.fields.FloatField')()),
            ('loanLimit', self.gf('django.db.models.fields.FloatField')()),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('service', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('lead_conversion', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
        ))
        db.send_create_signal(u'api', ['User'])

        # Adding M2M table for field groups on 'User'
        m2m_table_name = db.shorten_name(u'api_user_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'api.user'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'User'
        m2m_table_name = db.shorten_name(u'api_user_user_permissions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'api.user'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'permission_id'])

        # Adding M2M table for field followers on 'User'
        m2m_table_name = db.shorten_name(u'api_user_followers')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_user', models.ForeignKey(orm[u'api.user'], null=False)),
            ('to_user', models.ForeignKey(orm[u'api.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_user_id', 'to_user_id'])

        # Adding model 'UserProfile'
        db.create_table(u'api_userprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['api.User'])),
        ))
        db.send_create_signal(u'api', ['UserProfile'])

        # Adding model 'Post'
        db.create_table(u'api_post', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(related_name='posts', to=orm['api.User'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('body', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'api', ['Post'])

        # Adding model 'Photo'
        db.create_table(u'api_photo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('post', self.gf('django.db.models.fields.related.ForeignKey')(related_name='photos', to=orm['api.Post'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'api', ['Photo'])

        # Adding model 'Leads'
        db.create_table(u'api_leads', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('middle_init', self.gf('django.db.models.fields.CharField')(max_length=1, blank=True)),
            ('street_addr1', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('street_addr2', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('Zip', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('social_security', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('phone_home', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('phone_cell', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('phone_work', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('phone_work_ext', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Email', self.gf('django.db.models.fields.EmailField')(max_length=50)),
            ('birth_date', self.gf('django.db.models.fields.DateField')(null=True)),
            ('employer_name', self.gf('django.db.models.fields.CharField')(max_length=35, null=True)),
            ('pay_frequency', self.gf('django.db.models.fields.CharField')(max_length=12, null=True)),
            ('direct_deposit', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('pay_day1', self.gf('django.db.models.fields.DateField')(null=True)),
            ('pay_day2', self.gf('django.db.models.fields.DateField')(null=True)),
            ('bank_aba', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('bank_account', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('bank_name', self.gf('django.db.models.fields.CharField')(max_length=30, null=True)),
            ('income_monthly', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('own_home', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('drivers_license', self.gf('django.db.models.fields.CharField')(max_length=25, null=True)),
            ('drivers_license_st', self.gf('django.db.models.fields.CharField')(max_length=2, null=True)),
            ('client_url_root', self.gf('django.db.models.fields.URLField')(max_length=100, null=True)),
            ('client_ip_address', self.gf('django.db.models.fields.IPAddressField')(default=None, max_length=15, null=True)),
            ('email_alternate', self.gf('django.db.models.fields.EmailField')(max_length=100, null=True, blank=True)),
            ('months_employed', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('income_type', self.gf('django.db.models.fields.CharField')(max_length=15, null=True)),
            ('is_military', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('bank_account_type', self.gf('django.db.models.fields.CharField')(max_length=10, null=True)),
            ('requested_amount', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('months_at_address', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('months_at_bank', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('source', self.gf('django.db.models.fields.CharField')(max_length=10, null=True)),
            ('offered', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('user_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('offered_to', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
        ))
        db.send_create_signal(u'api', ['Leads'])

        # Adding model 'StatusCodes'
        db.create_table(u'api_statuscodes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='name')),
            ('value', self.gf('django.db.models.fields.PositiveIntegerField')(db_column='value')),
        ))
        db.send_create_signal(u'api', ['StatusCodes'])

        # Adding model 'LeadStatus'
        db.create_table(u'api_leadstatus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lead_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['api.Leads'], db_column='lead_id')),
            ('status_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['api.StatusCodes'], db_column='status_id')),
        ))
        db.send_create_signal(u'api', ['LeadStatus'])


    def backwards(self, orm):
        # Deleting model 'User'
        db.delete_table(u'api_user')

        # Removing M2M table for field groups on 'User'
        db.delete_table(db.shorten_name(u'api_user_groups'))

        # Removing M2M table for field user_permissions on 'User'
        db.delete_table(db.shorten_name(u'api_user_user_permissions'))

        # Removing M2M table for field followers on 'User'
        db.delete_table(db.shorten_name(u'api_user_followers'))

        # Deleting model 'UserProfile'
        db.delete_table(u'api_userprofile')

        # Deleting model 'Post'
        db.delete_table(u'api_post')

        # Deleting model 'Photo'
        db.delete_table(u'api_photo')

        # Deleting model 'Leads'
        db.delete_table(u'api_leads')

        # Deleting model 'StatusCodes'
        db.delete_table(u'api_statuscodes')

        # Deleting model 'LeadStatus'
        db.delete_table(u'api_leadstatus')


    models = {
        u'api.leads': {
            'Email': ('django.db.models.fields.EmailField', [], {'max_length': '50'}),
            'Meta': {'object_name': 'Leads'},
            'Zip': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'bank_aba': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'bank_account': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'bank_account_type': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'bank_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True'}),
            'birth_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'client_ip_address': ('django.db.models.fields.IPAddressField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'client_url_root': ('django.db.models.fields.URLField', [], {'max_length': '100', 'null': 'True'}),
            'direct_deposit': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'drivers_license': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True'}),
            'drivers_license_st': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True'}),
            'email_alternate': ('django.db.models.fields.EmailField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'employer_name': ('django.db.models.fields.CharField', [], {'max_length': '35', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'income_monthly': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'income_type': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'}),
            'is_military': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'middle_init': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'}),
            'months_at_address': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'months_at_bank': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'months_employed': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'offered': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'offered_to': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'own_home': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'pay_day1': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'pay_day2': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'pay_frequency': ('django.db.models.fields.CharField', [], {'max_length': '12', 'null': 'True'}),
            'phone_cell': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'phone_home': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'phone_work': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'phone_work_ext': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'requested_amount': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'social_security': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'source': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'street_addr1': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'street_addr2': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'})
        },
        u'api.leadstatus': {
            'Meta': {'object_name': 'LeadStatus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lead_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Leads']", 'db_column': "'lead_id'"}),
            'status_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.StatusCodes']", 'db_column': "'status_id'"})
        },
        u'api.photo': {
            'Meta': {'object_name': 'Photo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'post': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photos'", 'to': u"orm['api.Post']"})
        },
        u'api.post': {
            'Meta': {'object_name': 'Post'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'posts'", 'to': u"orm['api.User']"}),
            'body': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'api.statuscodes': {
            'Meta': {'object_name': 'StatusCodes'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'name'"}),
            'value': ('django.db.models.fields.PositiveIntegerField', [], {'db_column': "'value'"})
        },
        u'api.user': {
            'EinNumber': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'Meta': {'object_name': 'User'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'entityname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'followees'", 'symmetrical': 'False', 'to': u"orm['api.User']"}),
            'group': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {}),
            'lead_conversion': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'}),
            'loanLimit': ('django.db.models.fields.FloatField', [], {}),
            'longitude': ('django.db.models.fields.FloatField', [], {}),
            'managerEmail': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'managerName': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'managerPhone': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ownerEmail': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'ownerName': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'ownerPhone': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone_number': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'service': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'storeAddress': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'storeName': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'zip_code': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'api.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.User']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'generic_positions.objectposition': {
            'Meta': {'object_name': 'ObjectPosition'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'user_media.usermediaimage': {
            'Meta': {'object_name': 'UserMediaImage'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'thumb_h': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'thumb_w': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'thumb_x': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'thumb_x2': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'thumb_y': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'thumb_y2': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.User']"})
        }
    }

    complete_apps = ['api']