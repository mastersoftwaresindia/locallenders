from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes import generic

class User(AbstractUser):
	Converted=(
	('0','0'),
	('1','1')
	)
	followers = models.ManyToManyField('self', related_name='followees', symmetrical=False)
	phone_number = models.CharField(max_length=255,blank=True,null=True)
	zip_code = models.PositiveIntegerField(_("zip_code"),blank=True,null=True)
	group = models.CharField(max_length=255)
	entityname = models.CharField(max_length=255)
	state = models.CharField(max_length=255)
	ownerName = models.CharField(max_length=255)
	ownerEmail = models.CharField(max_length=255)
	ownerPhone = models.PositiveIntegerField(_("ownerPhone"))
	storeName = models.CharField(max_length=255)
	storeAddress = models.CharField(max_length=255)
	EinNumber = models.CharField(max_length=255,blank=True,null=True)
	managerName = models.CharField(max_length=255)
	managerEmail = models.CharField(max_length=255)
	managerPhone = models.CharField(max_length=255,blank=True,null=True)
	longitude = models.FloatField()
	latitude = models.FloatField()
	loanLimit =  models.FloatField(null=True)
	city = models.CharField(max_length=255)
	service = models.CharField(max_length=255)
	lead_conversion = models.PositiveIntegerField(_("lead_conversion"),blank=False,null=True,choices=Converted)
	
class UserProfile(models.Model):
    user = models.ForeignKey('api.User')
    avatar = generic.GenericRelation(
        'user_media.UserMediaImage',
    )


class Post(models.Model):
	author = models.ForeignKey(User, related_name='posts')
	title = models.CharField(max_length=255)
	body = models.TextField(blank=True, null=True)


class Photo(models.Model):
	post = models.ForeignKey(Post, related_name='photos')
	image = models.ImageField(upload_to="%Y/%m/%d")

'''Leads model to save lead data'''
class Leads(models.Model):
	Status = (
	(0, 'False'),
	(1, 'True')
	)

	IncomeType = (
	('EMPLOYMENT','EMPLOYMENT'),
	('BENEFITS','BENEFITS'),
	('SELFEMPLOYED','SELFEMPLOYED'),
	('UNEMPLOYED','UNEMPLOYED')
	)
	
	AccountType = (
	('CHECKING','CHECKING'),
	('SAVINGS','SAVINGS')
	)
	
	PayFrequency=(
	('WEEKLY','WEEKLY'),
	('BIWEEKLY','BIWEEKLY'),
	('TWICEMONTH','TWICEMONTH'),
	('MONTHLY','MONTHLY')
	)
	
	States = (
	('ALABAMA','AL'),
	('ALASKA','AK'),
	('ARIZONA','AZ'),
	('ARKANSAS','AR'),
	('CALIFORNIA','CA'),
	('COLORADO','CO'),
	('CONNECTICUT','CT'),
	('DELAWARE','DE'),
	('FLORIDA','FL'),
	('GEORGIA','GA'),
	('HAWAII','HI'),
	('IDAHO','ID'),
	('ILLINOIS','IL'),
	('INDIANA','IN'),
	('IOWA','IA'),
	('KANSAS','KS'),
	('KENTUCKY','KY'),
	('LOUISIANA','LA'),
	('MAINE','ME'),
	('MARYLAND','MD'),
	('MASSACHUSETTS','MA'),
	('MICHIGAN','MI'),
	('MINNESOTA','MN'),
	('MISSISSIPPI','MS'),
	('MISSOURI','MO'),
	('MONTANA','MT'),
	('NEBRASKA','NE'),
	('NEVADA','NV'),
	('NEW HAMPSHIRE','NH'),
	('NEW JERSEY','NJ'),
	('NEW MEXICO','NM'),
	('NEW YORK','NY'),
	('NORTH CAROLINA','NC'),
	('NORTH DAKOTA','ND'),
	('OHIO','OH'),
	('OKLAHOMA','OK'),
	('OREGON','OR'),
	('PENNSYLVANIA','PA'),
	('RHODE ISLAND','RI'),
	('SOUTH CAROLINA','SC'),
	('SOUTH DAKOTA','SD'),
	('TENNESSEE','TN'),
	('TEXAS','TX'),
	('UTAH','UT'),
	('VERMONT','VT'),
	('VIRGINIA','VA'),
	('WASHINGTON','WA'),
	('WEST VIRGINIA','WV'),
	('WISCONSIN','WI'),
	('WYOMING','WY'),
	)
	
	Offered=(
	('0','0'),
	('1','1')
	)

	first_name = models.CharField(max_length=50,blank=False,null=False)
	last_name = models.CharField(max_length=50,blank=False,null=False)
	middle_init = models.CharField(max_length=1,blank=True)
	street_addr1 = models.CharField(max_length=50,blank=False,null=False)
	street_addr2 = models.CharField(max_length=50,blank=True,null=False)
	city = models.CharField(max_length=100,blank=False,null=False)
	state = models.CharField(max_length=2,blank=False,null=False)
	Zip = models.PositiveIntegerField(_("Zip"),blank=False,null=True)
	social_security = models.PositiveIntegerField(_("social_security"),blank=False,null=True)
	phone_home = models.PositiveIntegerField(_("Home_Phone "),blank=False,null=True)
	phone_cell = models.PositiveIntegerField(_("phone_cell "),blank=True,null=True)
	phone_work = models.PositiveIntegerField(_("phone_work "),blank=False,null=True)
	phone_work_ext = models.PositiveIntegerField(_("phone_work_ext "),blank=True,null=True)
	Email = models.EmailField(max_length=50,blank=False,null=False)
	birth_date = models.DateField(blank=False,null=True)
	employer_name = models.CharField(max_length=35,blank=False,null=True)
	pay_frequency = models.CharField(max_length=12,blank=False,null=True,choices=PayFrequency)
	direct_deposit = models.PositiveIntegerField(_("direct_deposite"),blank=False,null=True)
	pay_day1 = models.DateField(blank=False,null=True)
	pay_day2 = models.DateField(blank=False,null=True)
	bank_aba = models.PositiveIntegerField(_("bank_aba"),blank=False,null=True)
	bank_account = models.PositiveIntegerField(_("bank_account"),blank=False,null=True)
	bank_name = models.CharField(max_length=30,blank=False,null=True)
	income_monthly = models.PositiveIntegerField(_("income_monthly"),blank=False,null=True)
	own_home = models.PositiveIntegerField(_("own_home"),blank=False,null=True,choices=Status)
	drivers_license =  models.CharField(max_length=25,blank=False,null=True)
	drivers_license_st = models.CharField(max_length=2,blank=False,null=True)
	client_url_root = models.URLField(max_length=100,blank=False,null=True)
	client_ip_address =  models.IPAddressField(null=True, blank=False, default=None)
	email_alternate = models.EmailField(max_length=100,blank=True,null=True)
	months_employed = models.PositiveIntegerField(_("months_employed"),blank=False,null=True)
	income_type = models.CharField(max_length=15,blank=False,null=True,choices=IncomeType)
	is_military = models.PositiveIntegerField(_("own_home"),blank=False,null=True,choices=Status)
	bank_account_type = models.CharField(max_length=10,blank=False,null=True,choices=AccountType)
	requested_amount = models.PositiveIntegerField(_("requested_amount"),blank=False,null=True)
	months_at_address = models.PositiveIntegerField(_("months_at_address"),blank=False,null=True)
	months_at_bank = models.PositiveIntegerField(_("months_at_bank"),blank=False,null=True)
	source = models.CharField(max_length=10,blank=False,null=True)
	offered = models.PositiveIntegerField(_("offered"),blank=False,null=True,choices=Offered)
	user_id = models.PositiveIntegerField(_("user_id"),blank=False,null=True)
	offered_to = models.PositiveIntegerField(_("offered_to"),blank=False,null=True)
	created_on = models.DateTimeField(blank=False,null=True)
	updated_on = models.DateTimeField(blank=True,null=True)


	

'''Status Codes Model'''
class StatusCodes(models.Model):
	Lead_Status = (
	('101','Have not gone yet'),
	('102','On my way'),
	('103','Got my Loan')
	)
	Lead_Status_Codes = (
	('101','101'),
	('102','102'),
	('103','103')
	)
	name = models.CharField(max_length=100,blank=False,null=False,choices=Lead_Status,db_column='name')
	value = models.PositiveIntegerField(_("value"),blank=False,null=False,choices=Lead_Status_Codes,db_column='value')




'''Lead-Status Model'''
class LeadStatus(models.Model):
	lead_id = models.PositiveIntegerField(_("value"),blank=False,null=True,db_column='lead_id')
	status_id = models.PositiveIntegerField(_("value"),blank=False,null=True,db_column='status_id')
	added_on = models.DateField(blank=False,null=True)
	updated_on = models.DateField(blank=True,null=True)
	


	


	

