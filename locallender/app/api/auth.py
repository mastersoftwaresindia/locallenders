from .models import User
from django.contrib.auth.hashers import *

class AlwaysRootBackend(object):
    def authenticate(self, *args, **kwargs):
        if 'username' in kwargs:
            uname = kwargs.pop('username')
            password = kwargs.pop('password')
            try:
                user = User.objects.get(username=uname)
                up = user.password
                uservalid = check_password(password,up)
                if uservalid == True or password == up:
                    return User.objects.get(username=uname)
                else:
                    print "No User Found"
            except Exception,e:
                print e
        else:
                print "here in auth.py"
                return User.objects.get(username='admin')
    
    def get_user(self, user_id):
        user = User.objects.get(id=user_id)
        return user
