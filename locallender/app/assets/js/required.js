jQuery(document).ready(function($){
    
    $("a.create").click(function(e){
        
        $("#signup").load('/create/', function(){
        
           $(this).lightbox_me({
            
                centered: true,
                onLoad: function() { 
                    $(this).find('.form input:first').focus()
                    $(this).removeClass('hide');
                    $('.close').click(function(){
                        
                        $(this).trigger('close');    
                        
                    })
                    $(".form").validate({
                        submitHandler: function(form) {
                            var opts = {
                                success: function(data) {
                                    
                                    $('#preview').text(data);
                                    
                                }
                            };
                            $(form).ajaxSubmit(opts);
                        }
                    });
                },
                closeClick: false,
                appearEase: "swing",
                
           }); 
        });
        e.preventDefault();
        
    });
    
    $("a.login").click(function(e){
        
        $("#signup").load('/login/', function(){
        
           $(this).lightbox_me({
            
                centered: true,
                onLoad: function() { 
                    $(this).find('.form input:first').focus()
                    $(this).removeClass('hide');
                    $('.close').click(function(){
                        
                        $(this).trigger('close');    
                        
                    })
                    $(".form").validate({
                        submitHandler: function(form) {
                            var opts = {
                                success: function(data) {
                                    
                                    $('#preview').text(data);
                                    
                                }
                            };
                            $(form).ajaxSubmit(opts);
                        }
                    });
                },
                closeClick: false,
                appearEase: "swing",
                
           }); 
        });
        e.preventDefault();
        
    });
    
});