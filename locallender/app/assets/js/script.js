if(window.location.href.indexOf("application") > -1) {
  if(document.cookie.indexOf("email") < 0){
    window.location.href = '/'    
  }
}
else if (window.location.href.indexOf("application") < 0){
    delete_cookie('email ')
}
var currentPath = window.location.href;

var lastChar = currentPath.substr(-1); // Selects the last character
if (lastChar != '/') {         // If the last character is not a slash
  $("#lender_link").hide();
}else{
  $("#lender_link").show();
}


(function() {
  var app;

  app = angular.module('app.app.static', [])
  .config(function($routeProvider,$locationProvider){
    
      $locationProvider.html5Mode(true);
  });
  app.controller('AppController', [
    '$scope', '$http','$location','$route','$window', function($scope, $http,$location,$route,$window) {
      var cookie = document.cookie;
      var cookievalue = read_cookie('email');
      $('#useremail').val(cookievalue);
      $scope.submit = function(){
        
        var csrf = $('input[type=hidden]').val();
        var email = this.email;
        var valid = validateEmail(email);
        document.cookie= "email =" + email +("; path=/");
        if (valid == true) {
            var user = {
                email:email,
                Token:csrf,
            }
            $http({ 
              method: 'POST', 
              url: 'api/customerexists', 
              data: {userEmail:email},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("response",data);
                if (data.result == 'success') {
                    $('.oauthStatus').html("Please wait....").promise().done(function(){
                      $('.oauthStatus').html("Redirect to application page");
                      $window.location.href="/application";
                    });
                }else{
                  $('.oauthStatus').html(data.detail + ".Please Click <a href='/accounts/login/' target='_self'>here</a> to login");
                }
            }).error(function(data, status, headers, config) {
              $('#authStaffStatus').html("Please wait....").promise().done(function(){
                $('#authStaffStatus').html(data);
              });
            });
        }else{
          $('.oauthStatus').html("Error");
        }
      };
    }
      
  ]);
  app.controller('StaffController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $scope.submit = function(person, resultVarName) {
        $http({ 
              method: 'POST', 
              url: 'api/staff', 
              data: {staffEmail:this.staffEmail,staffPassword:this.staffPassword,staffUsername:this.staffUsername},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("response",data);
                $('#authStaffStatus').html("No Staff Member found with these credentials").promise().done(function(){
                if (!data.error) {
                    $('#authStaffStatus').html(data);
                    $window.location.href = data;
                }else{
                  console.log("You are not a staff member");
                }
                
              });
            }).error(function(data, status, headers, config) {
              $('#authStaffStatus').html("Please wait....").promise().done(function(){
                $('#authStaffStatus').html(data);
              });
            });
      };
    }
  ]);
  app.controller('CustomerSignUpController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $("#customerPhoneNumber,#customerZipCode").on('keyup',function(e) {
              this.value = (this.value.replace(/[^0-9.\,]/g, ''));
       });
       $("#customerFirstName,#customerLastName").on('keyup',function(e) {
              this.value = (this.value.replace(/[^a-zA-Z_ \,]/g, ''));
       });
      $scope.submit = function() {
        var useremail = $('#useremail').val();
        var group = "Customers"
        if (this.customerFirstName && this.customerLastName && this.customerPhoneNumber && this.customerZipCode ) {
        $("#loader_app").show();
        $http({ 
              method: 'POST', 
              url: 'api/customer', 
              data: {first_name:this.customerFirstName,last_name:this.customerLastName,
              phone_number:this.customerPhoneNumber,zip_code:this.customerZipCode,
              customerEmail:useremail,group:group},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
              //$('#authStaffStatus').html("Please wait....").promise().done(function(){
                console.log(data);
                if (data.result == "success") {
                  $("#customersignup-form").hide();
                  $("#loader_app").hide();
                  var form = document.getElementById("customersignup-form");
                  form.reset();
                  $('#authStaffStatus').html(data.details);
                }else{
                  $("#loader_app").hide();
                  $('#authStaffStatus').html(data.details);
                }
            }).error(function(data, status, headers, config) {
              //$('#authStaffStatus').html("Please wait....").promise().done(function(){
               // $('#authStaffStatus').html(data);
              //});
            });
        }
      }
    }
    ]);
  
  app.controller('LenderSignUpController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
       $("#phoneNumber,#lenderZipcode").on('keyup',function(e) {
              this.value = (this.value.replace(/[^0-9.\,]/g, ''));
       });
       $("#lendername,#city,#storename").on('keyup',function(e) {
              this.value = (this.value.replace(/[^a-zA-Z_ \,]/g, ''));
       });
       $("#loanLimit").on('keyup',function(e) {
              this.value = (this.value.replace(/^(0*)/,""));
       });

      
      $scope.submit = function() {
        var group = "Lenders"
        console.log("data",this.lendername,this.password,this.email,this.confirmPassword,this.entityname,this.state,
        this.phoneNumber,this.storeName,this.city,this.storeAddress,this.lenderZipcode,this.service)
        if (this.password == this.confirmPassword) {
          $http({ 
              method: 'POST', 
              url: '/api/lendersignup', 
              data: {name:this.lendername,password:this.password,email:this.email,entityname:this.entityname,
              state:this.state,city:this.city,phoneNumber:this.phoneNumber,storeName:this.storeName,storeAddress:this.storeAddress,
              group:group,zipcode:this.lenderZipcode,service:this.service},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("here in success",data);
                if (data.result == 'success') {
                  $("#lendersignup-form").hide();
                  $('#resultStatus').html("Thanks for signing up! We will quickly verify your information and be in touch shortly.  Call us at any time to get started.");
                  var form = document.getElementById("lendersignup-form");
                  form.reset();
                }else{
                  $('#resultStatus').html(data.details);
                }
            }).error(function(data, status, headers, config) {
              console.log("here in error",data);
            });
        }else{
          $('#resultStatus').html("Your passwords didn't matched");
        }
      }
    }
  ]);
  
  app.controller('SignInController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $scope.submit = function() {
        var username = this.username;
        var password = this.password;
        $http({ 
              method: 'POST', 
              url: 'api/login', 
              data: {username:username,password:password},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                if (!data.error) {
                  $('#LoginStatus').html(data.detail);
                  $window.location.href="/customer-dashboard";
                  
                }else{
                  $('#LoginStatus').html('Please provide valid credentials');
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }
    }
  ]);
  
  //Controller to get the choosen lender by a customer
  app.controller('Selected-LenderController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $http({ 
              method: 'GET', 
              url: 'api/mylender', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                if (data.result == 'error') {
                  $('#lenderStatus').html(data.detail);
                }else{
                  $scope.Lender = data; 
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
    }
  ]);

  
  app.controller('ForgotPasswordController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $scope.submit = function() {
        var email = this.email;
        $http({ 
              method: 'POST', 
              url: 'api/forgotpassword', 
              data: {email:email},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
                if (data.result == 'success') {
                  $('#passwordStatus').html(data.detail);
                }else{
                  $('#passwordStatus').html('Please provide valid credentials');
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
        
      }
    }
  ]);

}).call(this);



////new added code for dashboard to get lenders list////

(function() {
var app;

  app = angular.module('app.app.dashboard', ['ui.bootstrap'])
  .config(function($routeProvider,$locationProvider){
    
      $locationProvider.html5Mode(true);
  });
  app.controller('CustomerDashboardController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $("#lenderStatus").hide();
      $http({ 
              method: 'GET', 
              url: 'api/getActiveLenders/', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                  $scope.lendersdata=data;
                  $scope.filteredlenders = [];
                  $scope.currentPage = 1;
                  $scope.numPerPage = 4;
                  $scope.maxSize = 3;
                  $scope.numPages = function () {
                    return Math.ceil($scope.lendersdata.length / $scope.numPerPage);
                  };
                
                  $scope.$watch('currentPage + numPerPage', function() {
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    ,end = begin + $scope.numPerPage;
                    $scope.filteredlenders = $scope.lendersdata.slice(begin, end);
                  });
              }).error(function(data, status, headers, config) {
              console.log("error");
              });
        
    
      $scope.chooseLender=function(lender_id){
        var lenderid = lender_id;
       $http({ 
              method: 'POST', 
              url: 'api/mylender', 
              data: {lenderid:lenderid},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
                if (data.result == 'success') {
                  $("#lenderStatus").show();
                  $('#lenderStatus').html(data.detail);
                  $('#lenderStatus').delay(3000).fadeOut('slow');
                }else{
                  $("#lenderStatus").show();
                  $('#lenderStatus').html(data.detail);
                  $('#lenderStatus').delay(3000).fadeOut('slow');
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
        
      
      }
    }
    
    ]);
  
  app.controller('LeadsListingController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $http({
              method: 'GET', 
              url: 'api/getMyLeads/', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                $('#leadStatus').html(data.detail);
                if (data.result == "success") { 
                  $scope.leadsdata=data.info;
                  $scope.filteredleads = [];
                  $scope.currentPage = 1;
                  $scope.numPerPage = 4;
                  $scope.maxSize = 3;
                  $scope.numPages = function () {
                    return Math.ceil($scope.leadsdata.length / $scope.numPerPage);
                  };
                
                  $scope.$watch('currentPage + numPerPage', function() {
                      var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                      ,end = begin + $scope.numPerPage;
                      $scope.filteredleads = $scope.leadsdata.slice(begin, end);
                  });
                }else{
                  $('#leadStatus').html(data.detail); 
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      //Date Filters
      $scope.eventDateFilter = function(column) {
        console.log("Date filter");
        /*if(column === 'all') {
            $scope.dateRange = '';
        } else if (column === 'lastweek') {
            var oneWeekAgo = new Date();
            oneWeekAgo.setDate(oneWeekAgo.getDate() - 7)
            console.log("oneweek",oneWeekAgo);
            function Lastweek(element) {
              console.log("element",element)
              var date = element.created_on
              console.log("date")
              return element.created_on == oneWeekAgo;
            }
var filtered = $scope.leadsdata.filter(Lastweek);
console.log("filtered",filtered);
        } else if (column === 'lasttwoweeks') {
            var twoWeekAgo = new Date();
            twoWeekAgo.setDate(twoWeekAgo.getDate() - 14)
            console.log("twoWeekAgo",twoWeekAgo);
             $scope.dateRange >= Date(twoWeekAgo);           
        } else if (column === 'lastmonth') {
            var lastmonth = new Date();
            lastmonth.setDate(lastmonth.getDate() - 30)
            console.log("lastmonth",lastmonth);
             $scope.dateRange = lastmonth;      
        } else if (column === 'twomonthsago') {
            var twomonthsago = new Date();
            twomonthsago.setDate(twomonthsago.getDate() - 60)
            console.log("twomonthsago",twomonthsago);
             $scope.dateRange = twomonthsago;   
        }else {
            $scope.dateRange = "";
        }*/
      };
      //ends here//
      $scope.leadStatus=function(status,id){
        var lead_status = status
        var lead_id = id
        $http({ 
              method: 'POST', 
              url: 'api/leadStatus/', 
              data: {lead_status :lead_status,lead_id : lead_id},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }
    
    }
  ]);
  
  app.controller('CustomerProfileController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $("#customerPhone,#phone_cell,#zip_code,#phone_work,#phonework_ext,#routing_number,#account_number,#monthly_income,#months_employed,#security_num,#requested_amount,#months_add,#months_bank").on('keyup',function(e) {
              this.value = (this.value.replace(/[^0-9.\,]/g, ''));
      });
      $("#first_name,#last_name,#city,#middle_init,#employer_name").on('keyup',function(e) {
              this.value = (this.value.replace(/[^a-zA-Z_ \,]/g, ''));
       });
      $http({
              method: 'GET', 
              url: 'api/getCustomerInfo', 
            }).success(function(data, status, headers, config) {
                console.log("data",data[0]);
                $scope.customerName = data[0].Name;
                $scope.customerPhone = data[0].Phone;
                $scope.customerEmail = data[0].Email;
                $scope.first_name = data[0].first_name;
                $scope.last_name = data[0].last_name;
                $scope.zip_code=data[0].zipCode
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      $scope.submit=function(){
        $http({ 
              method: 'POST', 
              url: 'api/saveLeadInfo', 
              data: {first_name:this.first_name,last_name:this.last_name,middle_init:this.middle_init,
              street_addr1:this.customerAddress1,street_addr2:this.customerAddress2,city:this.city,state:this.state,
              Zip:this.zip_code,social_security:this.security_num,phone_home:this.customerPhone,
              phone_cell:this.phone_cell,phone_work:this.phone_work,phone_work_ext:this.phonework_ext,
              Email:this.customerEmail,birth_date:this.birth_date,employer_name:this.employer_name,
              pay_frequency:this.pay_frequency,direct_deposite:this.direct_deposite,pay_day1:this.payday1,
              pay_day2:this.payday2,bank_aba:this.routing_number,bank_account:this.account_number,
              bank_name:this.bank_name,income_monthly:this.monthly_income,own_home:this.own_home,
              drivers_license:this.license_number,drivers_license_st:this.license_state,
              client_url_root:this.url_root,client_ip_address:this.ip_address,
              email_alternate:this.alternate_email,months_employed:this.months_employed,income_type:this.income_type,
              is_military:this.is_military,bank_account_type:this.account_type,requested_amount:this.requested_amount,
              months_at_address:this.months_add,months_at_bank:this.months_bank},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("here in success",data);
                if (data.result == 'success') {
                  //$("#leadInformation-form").hide();
                  $('#leadStatus').html("Thanks for Saving your information.You can choose a lender now");
                  //var form = document.getElementById("lendersignup-form");
                  //form.reset();
                }else{
                  $('#leadStatus').html(data.details);
                }
            }).error(function(data, status, headers, config) {
              console.log("here in error",data);
            });
      }
    }
  ]);
  
  app.controller('LenderProfileController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $("#saveStatus").hide(); 
      $("#owner_phone,#manager_phone,#loan_limit").on('keyup',function(e) {
              this.value = (this.value.replace(/[^0-9.\,]/g, ''));
      });
      $("#owner_name,#manager_name").on('keyup',function(e) {
              this.value = (this.value.replace(/[^a-zA-Z_ \,]/g, ''));
       });
      
      $http({
              method: 'GET', 
              url: 'api/getLenderInfo/', 
            }).success(function(data, status, headers, config) {
                console.log("data",data[0]);
                $scope.LenderInfo = data[0];
                $scope.owner_name=data[0].ownerName;
                $scope.owner_phone=data[0].ownerPhone;
                $scope.owner_email=data[0].ownerEmail;
                $scope.ein_number=data[0].EinNumber;
                $scope.manager_name=data[0].managerName;
                $scope.manager_phone=data[0].managerPhone;
                $scope.manager_email=data[0].managerEmail;
                $scope.loan_limit=data[0].loanLimit;
                $scope.store_phone = data[0].phone_number;
                $scope.store_email = data[0].email;
                $scope.store_address=data[0].storeAddress;
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      
      $scope.submit=function(){
        console.log(this.owner_name,this.owner_phone,this.owner_email,this.ein_number,this.manager_name,
                    this.manager_email,this.manager_phone,this.loan_limit,this.store_phone,this.store_address)
        $http({ 
              method: 'POST', 
              url: 'api/saveLenderInfo/', 
              data: {ownerName:this.owner_name,ownerEmail:this.owner_email,ownerPhone:this.owner_phone,
              EinNumber:this.ein_number,managerName:this.manager_name,managerEmail:this.manager_email,
              managerPhone:this.manager_phone,loanLimit:this.loan_limit,storeAddress:this.store_address,
              phone_number:this.store_phone},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.details);
                $('#saveStatus').show();
                $('#saveStatus').html(data.details);
                $('#saveStatus').delay(3000).fadeOut('slow');
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }
      
      /*$scope.makeRequest=function(){
        console.log("req",this.request)
         $http({ 
              method: 'POST', 
              url: 'api/lenderRequest/', 
              data: {'req':this.request},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }*/
    }
  ]);
  
  app.controller('CustomerListController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      customers_array=[]
      $http({ 
              method: 'GET', 
              url: 'api/getCustomers', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                $scope.customersdata=data;
                $scope.filteredcustomers = [];
                $scope.currentPage = 1;
                $scope.numPerPage = 5;
                $scope.maxSize = 3;
                $scope.numPages = function () {
                  return Math.ceil($scope.customersdata.length / $scope.numPerPage);
                };
                
                $scope.$watch('currentPage + numPerPage', function() {
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    ,end = begin + $scope.numPerPage;
                    $scope.filteredcustomers = $scope.customersdata.slice(begin, end);
                });
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
    
      $scope.approveLead = function(customer){
        customers_array.push(customer)
        //console.log("customer",customers_array)
      
      }
      
      $scope.convertToLead = function(){
          console.log("customer",customers_array)
          if (customers_array.length == 0) {
            $('#syncStatus').html('Please Choose customers to convert to lead');
          }else{
            customers_data=customers_array;
            $http({ 
              method: 'POST', 
              url: 'api/approveLead/', 
              data: {customers:customers_data,'flag':'staff'},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
                if (data.result == 'success') {
                  console.log(data.Details);
                  $('#syncStatus').html('Successfully Converted to lead');
                }else{
                  $('#syncStatus').html(data.details);
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
          }
      }
    }
  ]);
  
  app.controller('StaffDashboardController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $http({ 
              method: 'GET', 
              url: 'api/getLenders', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                $scope.lendersdata=data;
                $scope.filteredlenders = [];
                $scope.currentPage = 1;
                $scope.numPerPage = 4;
                $scope.maxSize = 3;
                $scope.numPages = function () {
                  return Math.ceil($scope.lendersdata.length / $scope.numPerPage);
                };
                
                $scope.$watch('currentPage + numPerPage', function() {
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    ,end = begin + $scope.numPerPage;
                    $scope.filteredlenders = $scope.lendersdata.slice(begin, end);
                });
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      
      $scope.approveLender = function(id,email){
      var lenderId = id;
      var lenderEmail = email;
      $http({ 
              method: 'POST', 
              url: 'api/approveLender/', 
              data: {lenderId:lenderId,lenderEmail:lenderEmail},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
                if (data.result == 'success') {
                  console.log(data.Details);
                }else{
                  console.log(data.error);
                  //$('#passwordStatus').html('Please provide valid credentials');
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      }
    }
  ])
}).call(this);
///ends here/////
  

(function() {
  var app;

  app = angular.module('app.app.editor', ['app.api', 'app.app.photos']);

  app.controller('EditController', [
    '$scope', 'Post', function($scope, Post) {
      $scope.newPost = new Post();
      return $scope.save = function() {
        return $scope.newPost.$save().then(function(result) {
          return $scope.posts.push(result);
        }).then(function() {
          return $scope.newPost = new Post();
        }).then(function() {
          return $scope.errors = null;
        }, function(rejection) {
          return $scope.errors = rejection.data;
        });
      };
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.manage', ['app.api', 'app.app.editor']);

  app.controller('DeleteController', [
    '$scope', 'AuthUser', function($scope, AuthUser) {
      $scope.canDelete = function(post) {
        return post.author.username === AuthUser.username;
      };
      return $scope["delete"] = function(post) {
        return post.$delete().then(function() {
          var idx;
          idx = $scope.posts.indexOf(post);
          return $scope.posts.splice(idx, 1);
        });
      };
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.photos', ['app.api']);

  app.controller('AppController', [
    '$scope', 'Post', 'PostPhoto', function($scope, Post, PostPhoto) {
      $scope.photos = {};
      $scope.posts = Post.query();
      return $scope.posts.$promise.then(function(results) {
        return angular.forEach(results, function(post) {
          return $scope.photos[post.id] = PostPhoto.query({
            post_id: post.id
          });
        });
      });
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.playground', ['app.api.playground']);

  app.controller('AppController', [
    '$scope', 'User', function($scope, User) {
      $scope.users = [];
      $scope.newUsername = "";
      $scope.loadUsers = function() {
        return User.query().$promise.then(function(results) {
          return $scope.users = results;
        });
      };
      $scope.addUser = function() {
        var user;
        user = new User({
          username: $scope.newUsername
        });
        $scope.newUsername = "";
        return user.$save().then($scope.loadUsers);
      };
      $scope.deleteUser = function(user) {
        return user.$delete().then($scope.loadUsers);
      };
      return $scope.loadUsers();
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.resource', ['app.api']);

  app.controller('AppController', [
    '$scope', 'Post', function($scope, Post) {
      return $scope.posts = Post.query();
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.bacic', []);
  var cookievalue = read_cookie('email');
  $('#useremail').val(cookievalue);
  app.controller('AppController', [
    '$scope', '$http', function($scope, $http) {
      $scope.posts = [];
      //return $http.post('/api/posts').then(function(result) {
      //  return angular.forEach(result.data, function(item) {
      //    return $scope.posts.push(item);
      //  });
      //});
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.update', ['app.api']);

  app.controller('AppController', [
    '$scope', 'User', function($scope, User) {
      $scope.users = [];
      $scope.newUsername = "";
      $scope.loadUsers = function() {
        return User.query().$promise.then(function(results) {
          return $scope.users = results;
        });
      };
      $scope.addUser = function() {
        var user;
        user = new User({
          username: $scope.newUsername
        });
        $scope.newUsername = "";
        return user.$save().then($scope.loadUsers);
      };
      $scope.deleteUser = function(user) {
        return user.$delete().then($scope.loadUsers);
      };
      return $scope.loadUsers();
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.api', ['ngResource']);

  app.factory('User', [
    '$resource', function($resource) {
      return $resource('/api/users/:username', {
        username: '@username'
      });
    }
  ]);

  app.factory('Post', [
    '$resource', function($resource) {
      return $resource('/api/posts/:id', {
        id: '@id'
      });
    }
  ]);

  app.factory('Photo', [
    '$resource', function($resource) {
      return $resource('/api/photos/:id', {
        id: '@id'
      });
    }
  ]);

  app.factory('UserPost', [
    '$resource', function($resource) {
      return $resource('/api/users/:username/posts/:id');
    }
  ]);

  app.factory('PostPhoto', [
    '$resource', function($resource) {
      return $resource('/api/posts/:post_id/photos/:id');
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.api.playground', []);

  app.factory('User', [
    '$q', function($q) {
      var MockUser, storage, user, username, _i, _len, _ref;
      storage = {};
      MockUser = (function() {
        function MockUser(params) {
          var key, value;
          for (key in params) {
            value = params[key];
            this[key] = value;
          }
        }

        MockUser.query = function() {
          var dfr, key, val, values;
          dfr = $q.defer();
          values = (function() {
            var _results;
            _results = [];
            for (key in storage) {
              val = storage[key];
              _results.push(val);
            }
            return _results;
          })();
          dfr.resolve(values);
          values.$promise = dfr.promise;
          return values;
        };

        MockUser.save = function(params) {
          var user;
          user = new this(params);
          user.$save();
          return user;
        };

        MockUser.prototype.$save = function() {
          var dfr;
          storage[this.username] = this;
          dfr = $q.defer();
          dfr.resolve(this);
          return dfr.promise;
        };

        MockUser.prototype.$delete = function() {
          var dfr;
          delete storage[this.username];
          dfr = $q.defer();
          dfr.resolve();
          return dfr.promise;
        };

        return MockUser;

      })();
      _ref = ['bob', 'sally', 'joe', 'rachel'];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        username = _ref[_i];
        user = new MockUser({
          username: username
        });
        storage[user.username] = user;
      }
      return MockUser;
    }
  ]);

}).call(this);

/*Function to validate an email address*/
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(email)){
        return true;
    }else{
        return false;
    }
} 

/*Function to delete cookie on url change*/
function delete_cookie( name ) {
  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

/*Function to read a cookie value*/
function read_cookie(key)
{
    var result;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
}

/*Function to validate a phonenumber*/
function phonenumber(inputtxt1,inputtxt2,inputtxt3)  
{  
  var phoneno = /^\d{10}$/;  
  if((inputtxt1.value.match(phoneno))&&(inputtxt2.value.match(phoneno))&&(inputtxt3.value.match(phoneno))){  
    var validphone = 'true'; 
  }else{  
    var validphone = 'false';
  }
  return validphone;
}  
