$(function() {
        
        $('body').on("click", '.change-card, .subscribe-form button[type=submit]', function(e) {
          e.preventDefault();
          var $form = $(this).closest("form"),
              token = function(res) {
                $form.find("input[name=stripe_token]").val(res.id);
                $form.trigger("submit");
              };

          StripeCheckout.open({
            key:         "pk_test_3j0y9oG0BYaFWw8DbkpRiXQt",
            name:        'LocalLenders.Net',
            panelLabel:  'Pay',
            amount :    '2000',
            token:       token
          });

          return false;
        });
    });

