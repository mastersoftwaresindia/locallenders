/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";

//App Controller starts here======================================**//
angular.module("myApp").controller("appcontroller", function ($scope,$http) {
    
    
    /*Created By : Sachin Sharma
     Dated: 16 Sept 2014
     Function to signup*/
    $scope.signup = function(){
        var csrf = $('input[type=hidden]').val();
        var email = $scope.email;
        var valid = validateEmail(email);
        if (valid == true) {
            var user = {
                email:email,
                Token:csrf,
            }
            $http({ 
                method: 'POST', 
                url: 'api/signup', 
                data: user,
                headers: {'Content-Type': 'application/json','X-CSRFToken': csrf}
            }). success(function(data, status, headers, config) {
                console.log('success');
            }).error(function(data, status, headers, config) {
                console.log('error');
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        }else{
            console.log("Please enter a valid email address")
        }
    };
});




/*Function to validate an email address*/
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(email)){
        return true;
    }else{
        return false;
    }
} 