 # A Python Calculator
###################################################################
#import the base module for gui i.e Tkinter
from Tkinter import *
top = Tk()
#adjusting  the window size
top.minsize(300,300)
top.geometry("200x200")
#Title of the window
top.title("Calculator")
#Frames for screens and bottons
screen=Frame(top,bd=1,width=150,height=25,relief=SUNKEN)
buttons=Frame(top,bd=1,width=5,height=2,relief=GROOVE)
screen.grid(column=0,row=0,padx=0,pady=0)
buttons.grid(column=0,row=1,padx=1)
screen.place(relx=0.15,rely=0.05)
buttons.place(rely=0.25,relx=0.02, height=180)

##FUNCTIONS For button clicks
def zero():
    results.insert(END,"0")

def clear():
    results.delete("0",END)

def Dot():
    results.insert(END,".")


def appear(x):
    return lambda: results.insert(END, x)

def Equals():
    try:
        result = eval(results.get())
    except:
        result = "Invalid sum"
    results.delete(0, END)
    results.insert(0, result)

def switch():
    top.geometry("200x600")
    advance=Frame(top,bd=1,width=150,height=25,relief=SUNKEN)
    advance.grid(column=0,row=1,padx=1)
    advance.place(rely=0.6,relx=0.15, height=140,width=220)
    def remove():
        advance.destroy()
        graph.destroy()
        top.geometry("200x200")

    def drawg():
        print "n1"

    def graph():
        root = Tk()
        #adjusting  the window size
        root.minsize(300,300)
        root.geometry("150x150")
        #Title of the window
        root.title("Graph")
        
        l1 = Label(root, text="Enter  the values for x")
        l1.pack()
        n1 = StringVar()
        r1 = Entry(root, textvariable=n1, width=20, fg="#003366",
        bg="#f2f2f2", font="Verdana")
        r1.pack()

        l2 = Label(root, text="Enter  the values for y")
        l2.pack()
        n2 = StringVar()
        r2 = Entry(root, textvariable=n2, width=20, fg="#003366",
        bg="#f2f2f2", font="Verdana")
        r2.pack()

        draw= Button(root, bg="White", text="Generate Graph ", width=10, height=1,command=drawg,
        relief=GROOVE)
        draw.grid(padx=2, pady=2, column=1, row=3)
        draw.pack()

    def output():
        import math
        r =results.get()
        s=r.split('(',1)
        t=s[1]
        u=t.split(')',1)
        v=u[0]
        w=float(v)
        try:
            if s[0]=='sin':
                x=math.sin(math.radians(w))
            elif s[0]=='cos':
                x=math.cos(w)
            elif s[0]=='tan':
                x=math.tan(math.radians(w))
            elif s[0]=='cosec':
                x=1/math.sin(math.radians(w))
            elif s[0]=='sec':
                x=1/math.cos(math.radians(w))
            elif s[0]=='cot':
                x=1/math.tan(math.radians(w))
            elif s[0]=='log':
                x=math.log(w)
            else:
                x="invalid"
        except:
            x = "Invalid"
        results.delete(0, END)
        results.insert(0, x)
    #Graph Button
    graph= Button(top, bg="White", text="Click here to go to Graph generator", width=25, height=1,command=graph,
    relief=GROOVE)
    graph.grid(padx=2, pady=2, column=1, row=3)
    graph.place(relx=0.1,rely=0.9)

    #advance function buttons    
    advfunctions=["sin","tan","cos","cosec","sec","cot","log","(",")"]
    for x in range(9):
        a=advfunctions[x]
        Button(advance, bg="White", text=a, width=5, height=1,command=appear(a),
        relief=GROOVE).grid(padx=2, pady=2, row=x%3,
        column=x/3)
    #Buttons to hide the advanced frame and get result
    hide= Button(advance, bg="White", text="Hide", width=5, height=1,
    command=remove,relief=GROOVE)
    hide.grid(padx=2, pady=2, column=1, row=3)
    hide.place(relx=0.1,rely=0.75)

    Result= Button(advance, bg="White", text="Result", width=5, height=1,command=output,relief=GROOVE)
    Result.grid(padx=2, pady=2, column=1, row=3)
    Result.place(relx=0.5,rely=0.75)

#Buttons
numbers=["1","4","7","2","5","8","3","6","9"]
for i in range(9):
    n=numbers[i]
    Button(buttons, bg="White", text=n, width=5, height=1,command=appear(n),
    relief=GROOVE).grid(padx=2, pady=2, row=i%3,
    column=i/3) 

zero= Button(buttons, bg="White", text="0", width=5, height=1,command=zero,
relief=GROOVE)
zero.grid(padx=2, pady=2, column=1, row=3)

dot= Button(buttons, bg="White", text=".", width=5, height=1,command=Dot,
relief=GROOVE)
dot.grid(padx=2, pady=2, column=0, row=3)


#Function Buttons
functions=["/", "*", "-", "+"]
for index in range(4):
    f=functions[index]
    Button(buttons, bg="White", text=f, width=5, height=1,command=appear(f),
    relief=GROOVE).grid(padx=2, pady=2, row=index%4,
    column=3) 

equals= Button(buttons, bg="White", text="=", width=5, height=1,command=Equals, relief=GROOVE)
equals.grid(ipadx=2, pady=2, row=3, column=2)

switch= Button(buttons, bg="White", text="Advance Panel", width=10, height=1,command=switch,
relief=GROOVE)
switch.grid(padx=2, pady=2, column=0, row=4)
switch.place(relx=0.5,rely=0.8)

clear= Button(buttons, bg="White", text="clear", width=5, height=1,command=clear,
relief=GROOVE)
clear.grid(padx=2, pady=2, column=0, row=4)
clear.place(relx=0.15,rely=0.8)


#Entry
numbers = StringVar()
results = Entry(screen, textvariable=numbers, width=20, fg="#003366",
bg="#f2f2f2", font="Verdana")
results.pack(ipady=10)

top.mainloop()
###################################################################

