/*jslint node: true */
/*global angular */
"use strict";



 // Factory to hit All Leads related RESTFUL API===========================*****//

LeadService.factory("Lead",   function($resource) {

    return $resource('/api/leads/:id', {  // Lead API*****//
      id: '@id'
      }, {
          saveLead: {  
          method:"POST"
        },
         // getAllLeads: {  
         // method:"GET"
        //},
    });
})

LeadService.factory("GetLead",   function($resource) {

    return $resource('/api/getleads/:id', {  // Lead API*****//
      id: '@id'
      }, {
          getAllLeads: {  
          method:"POST"
        }
    });
})

LeadService.factory("TodaysLeads",   function($resource) {

    return $resource('/api/todaysleads/:id', {  // Lead API*****//
      id: '@id'
      }, {
          getTodaysLeads: {  
          method:"POST"
        }
    });
})

LeadService.factory("PrevSevenDaysLeads",   function($resource) {

    return $resource('/api/prevsevendaysleads/:id', {  // Lead API*****//
      id: '@id'
      }, {
          getPrevSevenDaysLeads: {  
          method:"POST"
        }
    });
})

LeadService.factory("LeadDelete",   function($resource) {

    return $resource('/api/leadsdelete/:JobId', {  // Lead API*****//
      JobId: '@JobId'
      }, {
          deleteLead: {  
          method:"DELETE"
        }
    });
})

LeadService.factory("EditLead",   function($resource) {

    return $resource('/api/editleads/:JobId', {  // Lead API*****//
      JobId: '@JobId'
      }, {
          getEditLead: {  
          method:"POST"
        }
    });
})

LeadService.factory("LeadUpdate",   function($resource) {

    return $resource('/api/leadsupdate/:JobId', {  // Lead API*****//
      JobId: '@JobId'
      }, {
          updateLead: {  
          method:"POST"
        }
    });
})