/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";

angular.module("myApp.controllers").controller("DeveloperCtrl", function($scope, Developer,$http) {

// //TO get all the Developers=================================================================
       $scope.GetDeveloper = function(query) {
    return $http.get('api/developers?key='+query);
  }; 


});