/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";



angular.module("myApp.controllers").controller("scriptCtrl", function($scope, SaveKeyword,SaveDevelopers,ProjectKeyword,GetKeywordsByKey,Project,$http,$timeout) {
       

        // Created By: 53e366acec785106efe50bc8
        // Date: 08/08/2014
        // Puropse: TO get all the Keywords

       $scope.ImportKeywords = function() {

           var uniqueKeywords=[];
        $http.get('app/script/keywords.json').success(function(data) {
                    
              console.log('data :', data.length);
               for(var i=0;i<data.length;i++)
               {
                  uniqueKeywords.push(data[i].name);
               }

                uniqueKeywords=uniqueKeywords.filter(function(itm,m,a){
                        return m==uniqueKeywords.indexOf(itm);

                    });

                 //Here you will get all Unique Keywords
            for(var p=0;p<uniqueKeywords.length;p++){
                      
                           var obj=new SaveKeyword();
                          	obj.text=uniqueKeywords[p];
                         	 obj.$saveKeywords(function(response){
                           
                          		 });
                        console.log(p);

            }
           
     
            }).error(function(error) {
            	alert(error);
            });

}




        // Created By: 53e366acec785106efe50bc8
        // Date: 08/08/2014
        // Puropse: TO Import  All Projects
        
       $scope.ImportProjects = function() {

        $http.get('app/script/newproject.json').success(function(data) {
                   
         
               for(var i=0;i<data.length;i++)
               {
              
                 var keywords=[];
               	 var project = new Project();
               	 project.Title = data[i].TITLE;
		         project.Url = data[i].URL;
		         project.GitUrl = data[i].GithubUrl;
		         project.Description = data[i].DESC;
		         project.CreatedDate =  data[i].CREATEDDATE;
		         project.ModifyDate =  data[i].MODIFYDATE;
		         project.KeywordsId =  data[i].KEYS;
		         project.DevelopersId =  data[i].DEVELOPER;
		     

           project.$addProject(function (response) {

                if (response.status != 'false') {
                	console.log(response);

                }


            });
					
 }
     
            }).error(function(error) {
            	alert(error);
            });

}


$scope.ImportKeywordId=function(){
   // $scope.delay;
	var obj=new Project();
	obj.$projectList(function(response){

	  if (response.status != 'false') {

		for(var i=0;i<response.project.length;i++)
		{


          // console.log(response.project[i].DevelopersId);
			// Loop to hit all developer ID from JSON and then add them in ProjectKeywords schema            
                  // angular.forEach(response.project[i].DevelopersId, function (value, key) {
                  // 	if(response.project[i].DevelopersId[key]!=="")
                  // 	{
                  //    // console.log(response.project[i].DevelopersId[key]);
                  //      $scope.AddProjectDevelopers(response.project[i].id,response.project[i].DevelopersId[key]);
                  //      console.log("Developers Added Succes");
                  // 	}
                   

                  // });

           ////Loop to hit all keywords ID from JSON and then add them in ProjectKeywords schema
                 // angular.forEach(response.project[i].KeywordsId, function (value, key) {
                 // 	//console.log("kdfjk",response.project[i].id);
                 //    $scope.AddProjectKeywords(response.project[i].KeywordsId[key],response.project[i].id);
                 //        console.log("Keywords Added Succes");
                 // });
	
		   }
        }

	});


}



    // Created By: 53e366acec785106efe50bc8
    // Date: 08/07/2014
    // Puropse:Callback function to get Keywords
 $scope.GetKeywords = function( id ,callback) {
                          
                       var array=[]; 
                        var j=0;
                        for(var i=0;i<id.length;i++)
                         { 
                         	var keyobj=new GetKeywordsByKey()
		     	            keyobj.text=id[i];

                        keyobj.$getKeywordsByKeyName(function(response){

                            array.push(response.keywordList[0])
                            j=j+1;

                                if(j==id.length)
                                 {
                              
                                   callback(array);

                                 }

                            });
                          
                         }
                            
                  }


 // Created By: 53e366acec785106efe50bc8
    // Date: 10/07/2014
    // Puropse: function to Add a New Keywords

    $scope.AddProjectKeywords = function (keyword_Id, project_Id) {

        var projectKeyword = new ProjectKeyword();

        projectKeyword.projectId = project_Id;

        projectKeyword.keywordId = keyword_Id;

        projectKeyword.$addKeywords(function (response) {

        });

    };


    // Created By: 53e366acec785106efe50bc8
    // Date: 25/07/2014
    // Puropse: function to Add a New Developers

    $scope.AddProjectDevelopers = function (project_Id,developer_Id) {
       
        var obj = new SaveDevelopers();

        obj.projectId = project_Id;

        obj.developerId = developer_Id;

        obj.$addDevelopers(function (response) {

        });
    };



});


