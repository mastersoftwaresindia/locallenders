/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";

//Lead Controller starts here======================================**//

angular.module("myApp.controllers").controller("leadCtrl", function ($scope,GetLead,Lead,Client,LeadDelete,TodaysLeads,PrevSevenDaysLeads,EditLead,EditClient,LeadUpdate,ClientUpdate,$route,$rootScope,$location) {
		
$("#content").hide();

$(window).load(function(){
$(document).ready(function() {
var clicked = 0;
$('.lead-heading').on('click',function(){
if(clicked===0){
collapse();
clicked = 1;
}
else if(clicked===1){
expand();
clicked = 0;
}

})
function collapse(){

	$('#left').animate({
'width':'toggle'
},100);
$('#right').animate({
'width':'98%'
},100);
}
function expand(){
$('#left').animate({
'width':'toggle'
},100);
$('#right').animate({
'width':'98%'
},100);
}
});
});//]]>


	//Created By: Sachin Sharma
	//Date:08/08/2014
	//To Get Current Day Date
	var date1 = new Date();
	var date = date1.getDate();
  	var year = date1.getFullYear();
  	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";
	var month = month[date1.getMonth()]; 
	var CurrentDate = date+' '+month+' '+year;
	$scope.todayDate =  CurrentDate;


	//Created By: Sachin Sharma
	//Date:08/08/2014
	//To Get Current Time
	var date = new Date();
	var hours = date.getHours();
  	var minutes = date.getMinutes();
  	var ampm = hours >= 12 ? 'PM' : 'AM';
  	hours = hours % 12;
  	hours = hours ? hours : 12; // the hour '0' should be '12'
  	minutes = minutes < 10 ? '0'+minutes : minutes;
  	var strTime = hours + ':' + minutes + ' ' + ampm;
  	$scope.Time = strTime;

	//Created By: Sachin Sharma
	//Date:04/08/2014
	//Update lead and client information to database
	$scope.UpdateLead = function(JobId){
		var updatelead  = new LeadUpdate();
		var client = new ClientUpdate();
		//data to be updated in lead collection
		updatelead.JobTitle = $scope.jobtitle;
		updatelead.ApplicationUrl = $scope.appurl;
		updatelead.MessageUrl = $scope.msgurl;
		updatelead.Interviews = $scope.interviews;
		updatelead.BidAmount = $scope.amount;
		updatelead.LeadType = $scope.leadtype;
		updatelead.CurrentStatus = $scope.status;
		updatelead.ModifiedDate = new Date();
		updatelead.JobId =JobId;
 		//data to be updated in client collection
		client.ClientName = $scope.clientname;
		client.ClientEmail = $scope.clientemail;
		client.ClientBudget = $scope.clientbudget;
		client.ClientSkype = $scope.clientskype;
		client.ClientHistory = $scope.history;
		client.ModifiedDate = new Date(); 
		client.JobId = JobId;
		if(updatelead.JobTitle && updatelead.ApplicationUrl && updatelead.Interviews &&
			updatelead.BidAmount && updatelead.LeadType && updatelead.CurrentStatus &&
			client.ClientName && client.ClientBudget && client.ClientHistory){
				var url = validUrl(updatelead.ApplicationUrl);
				if (url == true){
	 				updatelead.$updateLead(function (response) {
	 					console.log("res-->",response);
						if (response.status == 'true') {
							console.log("Lead Updated successfully");
								client.$updateClient(function (response) {
	 								if (response.status == 'true') {
										console.log("Client Updated successfully");
										alert($rootScope.Lead_Edit_Msg);
									}else{
										console.log("error",response.error);
										alert($rootScope.invalid_DataType_Message);
										return false;
									}
								});
						}else{
							console.log("error",response.error);
							alert($rootScope.invalid_DataType_Message);
							return false;
						}
					});
				}
				else{
					alert($rootScope.invalid_Url_Message);
					return false;
				}
		}else{
			alert($rootScope.required_Fields_Msg);
			$scope.reqFieldsMsg = $rootScope.required_Fields_Msg;
			hideMessage("reqFieldsMsg");
		}
	}


	$scope.showallleadspaging = function(){
		$("#content").show();
		$("#myleadscontent").hide();
	}
	$scope.showmyleadspaging = function(){
		$("#content").hide();
		$("#myleadscontent").show();
	}
	

	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Edited Date:30/8/2014
	//Get all Leads from database
	$scope.getAllLeads = function(num){
		var getlead  = new GetLead();
		var pagenum = num;
		getlead.pagenum = pagenum;
		if($location.search().flag==null)
		{
		getlead.$getAllLeads(function (response) {
			if (response.status != 'false') {
				console.log("response<>>",response);
				var leadsCount = response.count;
				var pagenum = response.pagenum;
				if(pagenum !=0){
						var start = (50*pagenum)-50+1;
						var end = start+50;
						$scope.count = start;

					}else{
						$scope.count = 1;
					}
				var myleadscount = response.myleadscount;
				//var myleadscount = response.specific.length;
					if (myleadscount <=50){
						var leadpages = 1;
					}
					else{
						var leadpages =  Math.ceil(myleadscount/50);
					}
				//var startnum = response.skipcount;
				$scope.totalLeads = leadsCount;
				//console.log("leadsCount",leadsCount);
				var totalPages =  Math.ceil( leadsCount/50);
				$scope.pages = totalPages;
				if(pagenum == 0){
					$('#content').bootpag({
    				total: 5,
    				}).on("page", function(event, num){  $scope.getAllLeads(num)
    				$(this).bootpag({total: totalPages, maxVisible: 5});
     
    				});
    				$('#myleadscontent').bootpag({
    				total: 5
    				}).on("page", function(event, num){  $scope.getAllLeads(num)
    				$(this).bootpag({total: leadpages, maxVisible: 5});
     
    				});
				}
				var num = [];
				var a = 0;
				for(var i =0; i<totalPages;i++){
					a=i+1;
					num.push({"index":a});
				}
				$scope.number = num;
				$scope.allleads = response.result.allleads;
				var allleads = response.result.allleads;
				var leadslen = $scope.allleads.length;

				var specificLength = response.specific.length;
				$scope.myLeadsLen = myleadscount;
				$scope.specificLeads = response.specific;
			}else{
				console.log(response.error);
			}
		});
		}else if($location.search().flag=='prev'){
	  		$scope.getLastSevendaysLeads();
	  	}
	  	else
	  	{
	  		$scope.getTodaysLeads();
	  	}
	}

	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Get Today's Leads from database
	$scope.getTodaysLeads = function(num){
		var pagenum = num;
		var todaysleads  = new TodaysLeads();
		var date = new Date();
		todaysleads.today = date.toISOString();
		todaysleads.pagenum = pagenum;
		todaysleads.$getTodaysLeads(function (response) {
			 if (response.status != 'false') {
			 	//console.log("response",response);
			 	$scope.allleads = response.result.leads;
				var allleads = response.result.leads;
				var leadslen = $scope.allleads.length;
				$scope.totalLeads = leadslen;
				var biddername = response.uname;
				var pagenum = response.pagenum;
				if(pagenum !=0){
					var start = (50*pagenum)-50+1;
					var end = start+50;
					$scope.count = start;

				}else{
					$scope.count = 1;
				}

				var myleadscount = response.myleadscount;
				if (myleadscount >0 && myleadscount<=50){
					var leadspages = 2;
				}else if(myleadscount ==0){
					var leadspages = 0;
				}
				else{
					var leadspages = Math.ceil(myleadscount/50);
				}
				$scope.totalleads=response.leadscount;
				var leadscount = response.leadscount;
				console.log("leadscount",leadscount);
				//var totalPages = Math.ceil(bidscount/50);
				if (leadscount <=50){
					var totalPages = 2;
				}
				else{
					var totalPages = Math.ceil(myleadscount/50);
				}
				if(pagenum ==0){
				$('#content').bootpag({
    				total: 5,
    				}).on("page", function(event, num){  $scope.getTodaysLeads(num)
    				$(this).bootpag({total: totalPages, maxVisible: 5});
     
    				});
    				$('#myleadscontent').bootpag({
    				total: 5
    				}).on("page", function(event, num){  $scope.getTodaysLeads(num)
    				$(this).bootpag({total: leadspages, maxVisible: 5});
     
    				});
    			}
				var specificLength = myleadscount;
				$scope.myLeadsLen = specificLength;
				$scope.specificLeads = response.specificleads;
				$location.path('/recentleads').search({'flag': 'today'});
			}else{
				console.log(response.error);
			}
		});


	}	

	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Get Previous Seven Days leads from database
	$scope.getLastSevendaysLeads = function(num){
		var pagenum = num;
		var prevleads = new PrevSevenDaysLeads();
		prevleads.pagenum = pagenum;
		prevleads.$getPrevSevenDaysLeads(function (response) {
			console.log("sevendays leads",response);
			if (response.status != 'false') {
				$scope.allleads = response.result.leads;
				var allleads = response.result.leads;
				var leadslen = $scope.allleads.length;
				$scope.totalLeads = leadslen;
				var biddername = response.uname;
				var pagenum = response.pagenum;
				if(pagenum !=0){
					var start = (50*pagenum)-50+1;
					var end = start+50;
					$scope.count = start;

				}else{
					$scope.count = 1;
				}

				var myleadscount = response.myleadscount;
				if (myleadscount >0 && myleadscount<=50){
					var leadspages = 2;
				}else if(myleadscount ==0){
					var leadspages = 0;
				}
				else{
					var leadspages = Math.ceil(myleadscount/50);
				}
				$scope.totalleads=response.leadscount;
				var leadscount = response.leadscount;
				console.log("leadscount",leadscount);
				if (leadscount <=50){
					var totalPages = 2;
				}
				else{
					var totalPages = Math.ceil(myleadscount/50);
				}
				if(pagenum ==0){
				$('#content').bootpag({
    				total: 5,
    				}).on("page", function(event, num){  $scope.getLastSevendaysLeads(num)
    				$(this).bootpag({total: totalPages, maxVisible: 5});
     
    				});
    				$('#myleadscontent').bootpag({
    				total: 5
    				}).on("page", function(event, num){  $scope.getLastSevendaysLeads(num)
    				$(this).bootpag({total: leadspages, maxVisible: 5});
     
    				});
    			}
				var specificLength = myleadscount;
				$scope.myLeadsLen = specificLength;
				$scope.specificLeads = response.specificleads;
				$location.path('/recentleads').search({'flag': 'prev'});
			}else{
				console.log(response.error);
			}
		});

	}


	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Delete a specific lead from database
	$scope.DeleteLead = function(JobId,flag){
		var leadssdelete  = new LeadDelete();
		var flag = flag;
		leadssdelete.JobId = JobId;
		leadssdelete.flag = flag;
		leadssdelete.$deleteLead(function (response) {
			if (response.status != 'false') {
				$scope.leadDeleteMsg = $rootScope.Lead_Deletion_Msg;
			 	hideMessage("leadDeleteMsg");
			 	$scope.getAllLeads();
             } else {
                 alert(JSON.stringify(response));
             }
		});
		
	}

	//Created By: Sachin Sharma
	//Date:04/08/2014
	//Bind Data to edit form on a link click
	$scope.editLead = function(JobId){
		var editlead = new EditLead();
		var editclient = new EditClient();
		editlead.JobId = JobId;
		editclient.JobId = JobId;
		editlead.$getEditLead(function (response) {
			console.log("response lead",response.data[0]);
			$scope.jobtitle = response.data[0].JobTitle;
			$scope.appurl = response.data[0].ApplicationUrl;
			$scope.msgurl = response.data[0].MessageUrl;
			$scope.interviews = response.data[0].Interviews;
			$scope.amount = response.data[0].BidAmount;
			$scope.leadtype = response.data[0].LeadType;
			$scope.status = response.data[0].CurrentStatus;
			$scope.id = response.data[0].JobId;

		});
		editclient.$getEditClient(function (response) {
			console.log("response Client",response.data[0]);
			$scope.clientname = response.data[0].ClientName;
			$scope.clientemail = response.data[0].ClientEmail;
			$scope.clientbudget = response.data[0].ClientBudget;
			$scope.clientskype = response.data[0].ClientSkype;
			$scope.history = response.data[0].ClientHistory;
		});

	}


});

//Created By: Sachin Sharma
//Date:05/08/2014
// function to hide the messages after 4 seconds 
function hideMessage(id) {
  setTimeout(function () {
      $('#'+id).hide();
  }, 4000);
}



//Created By: Sachin Sharma
//Date:07/08/2014
// function to validate Url 
function validUrl(url) {
var url = url;
var rurl = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/
if ((rurl.test(url))){
	var res = true;
}else{
	var res = false;
}
return res;
}
