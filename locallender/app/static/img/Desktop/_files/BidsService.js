/*jslint node: true */
/*global angular */
"use strict";



 // Factory to hit All Bids related RESTFUL API===========================*****//

BidsService.factory("Bid",   function($resource) {

    return $resource('/api/bids/:id', {  // Bids API*****//
      id: '@id'
      }, {
          savebid: {  
          method:"POST"
        },
         // getAllBids: {  
          //method:"POST"
        //},
    });
})

BidsService.factory("GetBids",   function($resource) {

    return $resource('/api/getbids/:id', {  // Bids API*****//
      id: '@id'
      }, {
          getAllBids: {  
          method:"POST"
        },
         // getAllBids: {  
          //method:"POST"
        //},
    });
}) 
BidsService.factory("BidDelete",   function($resource) {

    return $resource('/api/bidsdelete/:JobId', {  // Bids API*****//
      JobId: '@JobId'
      }, {
          deletebid: {  
          method:"DELETE"
        }
    });
})

BidsService.factory("ChangeBidStatus",   function($resource) {

    return $resource('/api/changebidstatus/:JobId', {  // Bids API*****//
      JobId: '@JobId'
      }, {
          changestatus: {  
          method:"POST"
        }
    });
})

BidsService.factory("ChangeBidType",   function($resource) {

    return $resource('/api/changebidstype/:JobId', {  // Bids API*****//
      JobId: '@JobId'
      }, {
          changeToLead: {  
          method:"POST"
        }
    });
})

BidsService.factory("TodaysBids",   function($resource) {

    return $resource('/api/todaysbids/:id', {  // Bids API*****//
      id: '@id'
      }, {
          getTodaysBids: {  
          method:"POST"
        }
    });
})



BidsService.factory("PrevSevenDaysBids",   function($resource) {

    return $resource('/api/prevsevendaysbids/:id', {  // Bids API*****//
      id: '@id'
      }, {
          getPrevSevenDaysBids: {  
          method:"POST"
        }
    });
})