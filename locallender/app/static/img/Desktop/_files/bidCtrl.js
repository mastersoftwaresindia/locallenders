/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";

//Bid Controller starts here======================================**//
angular.module("myApp.controllers").controller("bidCtrl", function ($scope,Bid,GetBids,Lead,Client,BidDelete,ChangeBidStatus,ChangeBidType,TodaysBids,PrevSevenDaysBids,$route,$rootScope,$location) {

	$("#content1").hide();

	//Created By: Sachin Sharma
	//Date:08/08/2014
	//To Get Current Day Date
	var date1 = new Date();
	var date = date1.getDate();
  	var year = date1.getFullYear();
  	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";
	var month = month[date1.getMonth()]; 
	var CurrentDate = date+' '+month+' '+year;
	$scope.todayDate =  CurrentDate;


	//Created By: Sachin Sharma
	//Date:08/08/2014
	//To Get Current Time
	var date = new Date();
	var hours = date.getHours();
  	var minutes = date.getMinutes();
  	var ampm = hours >= 12 ? 'PM' : 'AM';
  	hours = hours % 12;
  	hours = hours ? hours : 12; // the hour '0' should be '12'
  	minutes = minutes < 10 ? '0'+minutes : minutes;
  	var strTime = hours + ':' + minutes + ' ' + ampm;
  	$scope.Time = strTime;


	//Created By: Sachin Sharma
	//Date:28/7/2014
	// Options for status
	$scope.Status = [
    	{
      		name: 'Applying',
      		value: 'Applying'
    	}, 
    	{
      		name: 'Applied',
      		value: 'Applied'
    	}
  	];
  
  	$scope.selectedstatus = $scope.Status[0].value;

	//Created By: Sachin Sharma
	//Date:28/7/2014
	// Options for IsInvite
  	$scope.invitation = [
    	{
      		name: 'No',
      		value: 'No'
    	}, 
    	{
      		name: 'Yes',
      		value: 'Yes'
    	}
  	];
  
  	$scope.selectedinvite = $scope.invitation[0].value;

	//Created By: Sachin Sharma
	//Date:31/7/2014
	// Options for LeadType
  	$scope.Type = [
    	{
      		name: 'Fixed',
      		value: 'Fixed'
    	}, 
    	{
      		name: 'Hourly',
      		value: 'Hourly'
    	}
  	];
  
  	$scope.leadtype = $scope.Type[0].value;


	//Created By: Sachin Sharma
	//Date:28/7/2014
	//To get the JobPortal type and jobid from url
	$scope.getJobPortalAndId = function(){
		var url = $scope.url;
		$scope.url = url.replace(/^\s+|\s+$/g,"");     
		if (typeof url == 'undefined'){
			$scope.portal = '';
			return false;
		}else{
			if(url.indexOf("odesk") > 0){
				$scope.portal = 'odesk';
			}else if (url.indexOf("elance") > 0){
				$scope.portal = 'elance';
			}else{
				$scope.portal = '';
			}
		}
			var id = getid(url);
			$scope.jobid = id;
	}


	//Created By: Sachin Sharma
	//Date:28/7/2014
	//Save Individual bid to database
	$scope.SaveBid = function(){
		var bids  = new Bid();
		bids.BidderName = $scope.biddername;
		bids.Technology = $scope.tech;
		bids.JobUrl = $scope.url;
		bids.JobPortal = $scope.portal;
		bids.JobId = $scope.jobid;
		bids.IsInvite = $scope.selectedinvite;
		bids.Status = $scope.selectedstatus;
		bids.BidType = "bid";
		bids.date = new Date();
		var joburl = $scope.url
		var url = validateUrl(joburl);
        if (url == true){
		if(bids.JobUrl && bids.JobId && bids.JobPortal && bids.IsInvite && bids.Technology && bids.JobId != 'null' && bids.JobPortal != 'null'){
        	$("#LoaderDiv").show();
			bids.$savebid(function (response) {
				if (response.status == 'true') {
              		$scope.bidaddMsg = $rootScope.Bid_Add_Msg;
              		document.getElementById("exampleInputEmail1").value = "";
              		document.getElementById("jobportal").value = "";
              		document.getElementById("jobid").value = "";
              		$scope.getAllBids();
              		//$scope.biddername=$rootScope.biddername;
            	} else {
            		$("#LoaderDiv").hide();
            		$scope.jobExistMsg = $rootScope.Job_Exist_Msg;
            		alert($scope.jobExistMsg);
            		hideMessage("jobExistMsg");
            		console.log(response.message);
            	}

			});
		 }else{
		 	$("#exampleInputEmail1").css("border-color","red");
		 	setTimeout(
        	function() { $("#exampleInputEmail1").css("border-color",""); },
        	5000
    );
			$scope.invalidMsg=$rootScope.invalid_Odesk_Url_Message;
	 		hideMessage("invalidMsg");
		 }
	}else{
			$scope.invalidMsg=$rootScope.invalid_Odesk_Url_Message;
			alert($scope.invalidMsg);
			$("#exampleInputEmail1").css("border-color","red");
			setTimeout(
        	function() { $("#exampleInputEmail1").css("border-color",""); },
        	5000
    );
		}
	}


	$scope.showallbidspaging = function(){
		$("#content1").show();
		$("#mybidscontent").hide();
	}
	$scope.showmybidspaging = function(){
		$("#content1").hide();
		$("#mybidscontent").show();
	}

	
	//Created By: Sachin Sharma
	//Date:28/7/2014
	//Edited Date:30/8/2014
	//Get all bids from database
	$scope.getAllBids = function(num){
		var getbids  = new GetBids();
		var pagenum = num;
		getbids.pagenum = pagenum;
		if($location.search().flag==null)
		{
			getbids.$getAllBids(function (response) {
				if (response.status != 'false') {
					console.log("response",response);
					var bidsCount = response.count;
					var mybidscount = response.mybidscount;
					var pagenum = response.pagenum;
					//var totalBidsdata = response.result.allbids;
					if(pagenum !=0){
						var start = (50*pagenum)-50+1;
						var end = start+50;
						$scope.count = start;

					}else{
						$scope.count = 1;
					}

					//var mybidscount = response.specific.length;
					if (mybidscount <=50){
						var bidspages = 1;
					}
					else{
						var bidspages = Math.ceil(mybidscount/50);
					}
					$scope.totalBids=bidsCount;
					console.log("bidsCount",bidsCount);
					var totalPages = Math.ceil(bidsCount/50);
					if(pagenum == 0){
					$('#content1').bootpag({
    					total: 5,
    					}).on("page", function(event, num){  $scope.getAllBids(num)
    					$(this).bootpag({total: totalPages, maxVisible: 5});
     
    				});
    				$('#mybidscontent').bootpag({
    					total: 5
    					}).on("page", function(event, num){  $scope.getAllBids(num)
    					$(this).bootpag({total: bidspages, maxVisible: 5});
     
    				});
				}
					var num = [];
					var a = 0;
					for(var i =0; i<totalPages;i++){
						a=i+1;
						num.push({"index":a});
					}
					$scope.number = num;
					//$scope.pages = totalPages;

					$scope.allbids = response.result.allbids;
					//$scope.allbids = totalBidsdata;
					$("#LoaderDiv").hide();
					var allbids = response.result.allbids;
					var bidslen = $scope.allbids.length;
					//$scope.totalBids = bidslen;
					var biddername = response.uname;
					$scope.biddername = response.uname;
					$rootScope.biddername = response.uname;
					if(biddername.indexOf('.') !== -1){
						var newName =biddername.split('.').join(" ");
					}else{
						var newName =biddername;
					}	
					var finalname = eachWord(newName);
					$rootScope.name = finalname;
					$scope.name  = $rootScope.name;
					var biddername = response.uname;
					var specificLength = response.specific.length;
					$scope.myBidsLen = mybidscount;
					$scope.specificBids = response.specific;
				}else{
					console.log(response.error);
				}
			});
	  	}else if($location.search().flag=='prev'){
	  		$scope.getLastSevendaysBids();
	  	}
	  	else
	  	{
	  	$scope.getTodaysBids();
	  	}
	}


	
	//Created By: Sachin Sharma
	//Date:29/7/2014
	//Delete a specific bids from database
	$scope.DeleteBid = function(){
		var JobId = document.getElementById('hidjob').value;
		var bidsdelete  = new BidDelete();
		bidsdelete.JobId = JobId;
		bidsdelete.$deletebid(function (response) {
			 if (response.status != 'false') {
			 	$scope.bidDeleteMsg = $rootScope.Bid_Deletion_Msg;
			 	hideMessage("bidDeleteMsg");
			 	$scope.getAllBids();
             } else {
                 alert(JSON.stringify(response));
             }

		});
	}


	
	
	//Created By: Sachin Sharma
	//Date:29/7/2014
	//Change Status for specific bids in database
	$scope.ChangeStatus = function(index,JobId,Status,flag){
		alert(index);
		//$(".spin .fa-li fa fa-spinner fa-spin").addClass('hell');
		return false;
		var flag = flag;
		var statchange  = new ChangeBidStatus();
		statchange.JobId = JobId;
		statchange.Status = Status;
		statchange.flag = flag;
		statchange.$changestatus(function (response) {
			if(response.status == 'true'){
				console.log("response",response);
				$scope.jobstatus = response.jobstatus;
				//$(".fa-li fa fa-spinner fa-spin").addClass('applied');
				$scope.bidStatChangeMsg = $rootScope.Bid_StatusChange_Msg;
				hideMessage("bidStatChangeMsg");
				console.log("Status Updated successfully")
				$scope.bidstat = $rootScope.bid_change;
				if(flag == 'today'){
					$scope.getTodaysBids();
				}
				else if(flag == 'prev'){
					$scope.getLastSevendaysBids();
				}else{
					$scope.getAllBids();
				}
			}else {
                 alert(JSON.stringify(response));
             }
		});
	}

	
	//Created By: Sachin Sharma
	//Date:30/7/2014
	//Get Today's bids from database
	$scope.getTodaysBids = function(num){
		var pagenum = num;
		$("#LoaderDiv").hide();
		var todaysbids  = new TodaysBids();
		var date = new Date();
		todaysbids.pagenum = pagenum;
		todaysbids.today = date.toISOString();
		todaysbids.$getTodaysBids(function (response) {
			if (response.status != 'false') {
				//console.log("response>>>>",response);
				$scope.allbids = response.result.bids;
				var allbids = response.result.bids;
				var bidslen = $scope.allbids.length;
				$scope.totalBids = response.bidscount;
				var pagenum = response.pagenum;
				var totalBidsdata = response.result.allbids;
				if(pagenum !=0){
					var start = (50*pagenum)-50+1;
					var end = start+50;
					$scope.count = start;

				}else{
					$scope.count = 1;
				}

				var mybidscount = response.mybidscount;
				if (mybidscount <=50){
					var bidspages = 2;
				}
				else{
					var bidspages = Math.ceil(mybidscount/50);
				}
				$scope.totalBids=response.bidscount;
				var bidscount = response.bidscount;
				console.log("bidscount",bidscount);
				//var totalPages = Math.ceil(bidscount/50);
				if (bidscount <=50){
					var totalPages = 2;
				}
				else{
					var totalPages = Math.ceil(mybidscount/50);
				}
				if(pagenum == 0){
					$('#content1').bootpag({
    					total: 5,
    					}).on("page", function(event, num){  $scope.getTodaysBids(num)
    					$(this).bootpag({total: totalPages, maxVisible: 5});
     
    					});
    				$('#mybidscontent').bootpag({
    					total: 5
    					}).on("page", function(event, num){  $scope.getTodaysBids(num)
    					$(this).bootpag({total: bidspages, maxVisible: 5});
     
    				});
    			}
				var biddername = response.uname;
				var specificLength = mybidscount;
				$scope.myBidsLen = specificLength;
				$scope.specificBids = response.specificbids;
				$location.path('/recentbids').search({'flag': 'today'});
				//console.log("$scope.specificBids--->",$scope.specificBids);
			}else{
				console.log(response.error);
			}

		});
	}



	//Created By: Sachin Sharma
	//Date:30/7/2014
	//Get Previous Seven Days bids from database
	$scope.getLastSevendaysBids = function(num){
		var pagenum = num;
		$("#LoaderDiv").hide();
		var prevbids = new PrevSevenDaysBids();
		prevbids.pagenum = pagenum;
		prevbids.$getPrevSevenDaysBids(function (response) {
			if (response.status != 'false') {
				console.log("response>>",response);
				$scope.allbids = response.result.bids;
				var allbids = response.result.bids;
				var bidslen = $scope.allbids.length;
				$scope.totalBids = response.bidscount;
				var pagenum = response.pagenum;
				var totalBidsdata = response.result.allbids;
				if(pagenum !=0){
					var start = (50*pagenum)-50+1;
					var end = start+50;
					$scope.count = start;

				}else{
					$scope.count = 1;
				}

				var mybidscount = response.mybidscount;
				if (mybidscount <=50){
					var bidspages = 2;
				}
				else{
					var bidspages = Math.ceil(mybidscount/50);
				}
				$scope.totalBids=response.bidscount;
				var bidscount = response.bidscount;
				console.log("bidscount",bidscount);
				//var totalPages = Math.ceil(bidscount/50);
				if (bidscount <=50){
					var totalPages = 2;
				}
				else{
					var totalPages = Math.ceil(mybidscount/50);
				}
				if(pagenum == 0){
					$('#content1').bootpag({
    					total: 5,
    					}).on("page", function(event, num){  $scope.getLastSevendaysBids(num)
    					$(this).bootpag({total: totalPages, maxVisible: 5});
     
    					});
    				$('#mybidscontent').bootpag({
    					total: 5
    					}).on("page", function(event, num){  $scope.getLastSevendaysBids(num)
    					$(this).bootpag({total: bidspages, maxVisible: 5});
     
    				});
    			}
				var specificLength = mybidscount;
				$scope.myBidsLen = specificLength;
				$scope.specificBids = response.specificbids;
				$location.path('/recentbids').search({'flag': 'prev'});
			}else{
				console.log(response.error);
			}

		});


	}
	

	//Created By: Sachin Sharma
	//Date:06/08/2014
	//Show bootstrap modal for tasks
	$scope.tasksModal = function(even,url,id){
		var url = url;
		var JobId = id;
		$scope.hidjob = id;
		$scope.hidurl = url;
		$('#myModal').modal('toggle');
	}

	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Show bootstrap modal to convert to lead
	$scope.lead = function(){
		$('#myModal').modal('hide');
		var JobId = document.getElementById('hidjob').value;
		var url = document.getElementById('hidurl').value;
		var JobId = JobId;
		$scope.hid = url;
		$scope.hidbtn = JobId;
		$('#myModal1').modal('toggle');
    

	}

	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Convert Bid To Lead
	$scope.ConvertToLead = function(joburl,jobid){
		var url = joburl;
		var lead  = new Lead();
		var client = new Client();
		var bidchange =new ChangeBidType();
		bidchange.JobId = jobid;
		//data to be saved in lead collection
		lead.JobId = jobid;
		lead.JobTitle = $scope.jobtitle;
		lead.ApplicationUrl = $scope.appurl;
		lead.MessageUrl = $scope.msgurl;
		lead.Interviews = $scope.interviews;
		lead.BidAmount = $scope.amount;
		lead.LeadType = $scope.leadtype;
		lead.CurrentStatus = $scope.status;
		lead.date = new Date();
		// 	//data to be saved in client collection
		client.JobId = jobid;
		client.ClientName = $scope.clientname;
		client.ClientEmail = $scope.clientemail;
		client.ClientBudget = $scope.clientbudget;
		client.ClientSkype = $scope.clientskype;
		client.ClientHistory = $scope.history;
		client.date = new Date();
		
		if(lead.JobTitle && lead.ApplicationUrl && lead.Interviews && lead.BidAmount && lead.LeadType && lead.CurrentStatus && client.ClientName && client.ClientBudget && client.ClientHistory){
			lead.$saveLead(function (response) {
				console.log(response);
				if (response.status == 'true') {
					console.log("Lead Added successfully");
				}else{
					console.log("error",response.error);
				}
			});
	
			client.$saveClient(function (response) {
				console.log(response);
				if (response.status == 'true') {
					console.log("Client Added successfully");
				}else{
					console.log("error",response.error);
				}
			});
			bidchange.$changeToLead(function (response) {
              	if (response.status == 'true') {
					console.log("Bid Type Changed successfully");
				
				}else{
					console.log("error",response.error);
				}

			});
			
			$('#myModal1').modal('hide');
			$scope.bidtoLeadMsg = $rootScope.Bid_ToLead_Msg;
			hideMessage("bidtoLeadMsg");
			$scope.getAllBids();
		}else{
			$scope.reqFieldsMsg = $rootScope.required_Fields_Msg;
			hideMessage("reqFieldsMsg");
		}
}
});



//Created By: Sachin Sharma
//Date:29/7/2014
// function to validate Url 
function validateUrl(url) {
var url = url;
var rurl= /^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/;  
if ((rurl.test(url))){
	var res = true;
}else{
	var res = false;
}
return res;
}




//Created By: Sachin Sharma
//Date:28/7/2014
// function to Get JobId from JobUrl 
function getid(value) {
var tilt = value.split('~');
var splitslash = tilt[0].split('/');
var test = (splitslash[2])
if(test == "www.odesk.com"){
    var check = value.split('/');
    var len =  check.length;
    var last =  (check[len-1]);
    var slast = (check[len-2]);
    if (slast == "applications"){
        return false;
    }
    var JobId = value.split('~');
            var id = JobId[1]
            var check1 =   (id.indexOf('?') > -1); //true
            var check2 =    (id.indexOf('/') > -1); //true
            if (check1){
                var Id = id.split('?');
                var JobId = Id[0];
            }
            if (check2){
                var Id = id.split('/');
                var JobId = Id[0];
            }
            else {
                var JobId = JobId[1];
            }

}
else if (test == "www.elance.com"){
        var regex = /\/\d+\//;
            var url = value;
            var exist = url.match(regex);
            var strid = String(exist);
            var JobId = strid.replace(/\//g,'');
            JobId = JobId;
}

return JobId;
}
//ends here




//Created By: Sachin Sharma
//Date:04/08/2014
// function to hide the messages after 4 seconds 
function hideMessage(id) {
  setTimeout(function () {
      $('#'+id).hide();
  }, 4000);
}


function eachWord(str){
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
