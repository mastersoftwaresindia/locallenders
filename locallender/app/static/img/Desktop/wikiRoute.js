var request= require('request');
var cheerio = require('cheerio');
var ld = require("levenshtein-damerau");
var natural = require('natural');
var connection;
// callback
 var myCallback = function(connection) {
   connection.connect(function(err) {
    if(err) {
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000);
    }
  });
   connection.on('error', function(err) {
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.log('event enter');
      usingItNow(myCallback);
    } else {
      throw err;
    }
  });
 };
 var usingItNow = function(callback) {
  connection = require('../db.js').handleDisconnect();
   callback(connection);
 };
 usingItNow(myCallback);

exports.wikiData = function(req , res){
  var url = req.body.path;
  request(url, function(err,resp,body)
  {
    if(!err && resp.statusCode == 200)
    {
      var $ = cheerio.load(body);
      title = $('title').text();
      var eventsW = [];
      var births = [];
      var deaths = [];
      var holidays = [];
      var holiday = $('#Holidays_and_observances').parent().next().text();
      var eventsHtml = $('#Events').parent().next().find('ul li').length;
      var eventsHtml1 = $('.thumb , .tright').next().find('ul li').length;
      var birthHtml = $('#Births').parent().next().find('ul li').length;
      var deathHtml = $('#Deaths').parent().next().find('ul li').length;
      var holidayHtml = $('#Holidays_and_observances').parent().next().find('ul li').length;
      console.log('holidayysssssssssssss',holidayHtml);
      var holidayData = holiday.split('\n');

      // events wiki item
      for(var i=0; i<eventsHtml; i++){
        var linksName = [];
        var eventsText = $('#Events').parent().next().find("li").eq(i).text();
        var eventData = eventsText.split('–');
        var splitYear = eventData[0].split(' ');
        if(splitYear[1])
          var yearC = '-'+splitYear[0];
        else
          var yearC = splitYear[0];
        var events = $('#Events').parent().next().find("li").eq(i).children('a').length;
        for(var a=0; a<events; a++){
          var urls = $('#Events').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
          urls = 'http://en.wikipedia.org'+urls;
          var name = $('#Events').parent().next().find("li").eq(i).children('a').eq(a).text();
          linksName.push({'url':urls,'name':name});
        }
        if(eventData[2]){
          var concatData = eventData[1]+'-'+eventData[2];
          eventsW.push({'event':concatData, 'year':yearC,'eventUrl':linksName});
        }else{
          eventsW.push({'event':eventData[1], 'year':yearC,'eventUrl':linksName});
        }
      }

      //for div inside wiki Events item
      if(eventsHtml1 > 0){
        console.log('find a div inside events wiki item ');
        for(var i=0; i<eventsHtml1; i++){
          var linksName = [];
          var eventsText = $('.thumb , .tright').next().find("li").eq(i).text();
          if(eventsText.search("–") > -1){
            var eventData = eventsText.split('–');
          }
          var splitYear = eventData[0].split(' ');
          if(splitYear[1])
            var yearC = '-'+splitYear[0];
          else
            var yearC = splitYear[0];
          var events = $('.thumb , .tright').next().find("li").eq(i).children('a').length;
          for(var a=0; a<events; a++){
            var urls = $('.thumb , .tright').next().find("li").eq(i).children('a').eq(a).attr('href');
            urls = 'http://en.wikipedia.org'+urls;
            var name = $('.thumb , .tright').next().find("li").eq(i).children('a').eq(a).text();
            linksName.push({'url':urls,'name':name});
          }
          if(eventData[2]){
            var concatData = eventData[1]+'-'+eventData[2];
            eventsW.push({'event':concatData, 'year':yearC,'eventUrl':linksName});
          }else{
            eventsW.push({'event':eventData[1], 'year':yearC,'eventUrl':linksName});
          }
        }
      }
    // births wiki item
    for(var i=0; i<birthHtml; i++){
      var linksName = [];
      var eventsText = $('#Births').parent().next().find("li").eq(i).text();
      if(eventsText.search("–") > -1){
        var eventData = eventsText.split('–');
      }
      var splitYear = eventData[0].split(' ');
      if(splitYear[1])
        var yearC = '-'+splitYear[0];
      else
        var yearC = splitYear[0];
      var nameVal = eventData[1].split(',');
      var events = $('#Births').parent().next().find("li").eq(i).children('a').length;

      for(var a=0; a<events; a++){
        var urls = $('#Births').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
        urls = 'http://en.wikipedia.org'+urls;

        var name = $('#Births').parent().next().find("li").eq(i).children('a').eq(a).text();
        linksName.push({'url':urls,'name':name});
      }

      births.push({'name':nameVal[0],'event':eventData[1], 'year':yearC,'birthUrl':linksName});
    }
    // death wiki items
    for(var i=0; i<deathHtml; i++){
      var linksName = [];
      var eventsText = $('#Deaths').parent().next().find("li").eq(i).text();
      if(eventsText.search("–") > -1){
        var eventData = eventsText.split('–');
      }
      var splitYear = eventData[0].split(' ');
      if(splitYear[1])
        var yearC = '-'+splitYear[0];
      else
        var yearC = splitYear[0];
      var nameVal = eventData[1].split(',');
      var events = $('#Deaths').parent().next().find("li").eq(i).children('a').length;
      for(var a=0; a<events; a++){
        var urls = $('#Deaths').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
        urls = 'http://en.wikipedia.org'+urls;
        var name = $('#Deaths').parent().next().find("li").eq(i).children('a').eq(a).text();
        linksName.push({'url':urls,'name':name});
      }
      deaths.push({'name':nameVal[0],'event':eventData[1], 'year':yearC,'birthUrl':linksName});
    }

    // holidays wiki item
    for(var i=0; i<holidayHtml; i++){
      var linksName = [];
      var eventsText = $('#Holidays_and_observances').parent().next().find("li").eq(i).text();
      var events = $('#Holidays_and_observances').parent().next().find("li").eq(i).children('a').length;
      for(var a=0; a<events; a++){
        var urls = $('#Holidays_and_observances').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
        urls = 'http://en.wikipedia.org'+urls;
        var name = $('#Holidays_and_observances').parent().next().find("li").eq(i).children('a').eq(a).text();
        linksName.push({'url':urls,'name':name});
      }
      holidays.push({'event':eventsText,'birthUrl':linksName});
    }
    /*for(var i=0; i<=holidayData.length; i++){
      if(holidayData[i]){
        holidays.push(holidayData[i]);
      }
    }*/
    res.json({ wikiEvents:eventsW , wikiDeaths: deaths , wikiHolidays:holidays , wikiBirths:births });
    //res.json({ exitHolidayEvents:exitHolidayEvents,exitBirthEvents:exitBirthEvents, exitDeathEvent: exitDeathEvents , NewHolidays:newHolidayEvents , internalHolidays:internalHolidayEvents,internalDeaths: internalDeathEvents , NewDeaths:newDeathEvents , internalBirths:internalBirthEvents,NewBirths:newBirthEvents,Events: events , internal:internalEvents,newevents:newArray,exitEvents:exitEvents});
      }
  });
};




//get events
exports.eventItem = function(req , res){
  var eventsW = req.body.wikiEvents;
  //console.log("wiki====>",eventsW);
  var internalEvents = [];
  var newArray = [];
  var exitEvents = [];
  if(req.body.day && req.body.month){
    var day = req.body.day;
    var month = req.body.month;
  }
  else{
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
  }
  //var sql = "SELECT * FROM events  WHERE day = '"+ day +"' AND month = '"+ month +"' AND category_id = '36'" ;
  //var sql = "SELECT e.event,e.year,e.id,el.link_id,l.url FROM events e,links l,events_links el WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = '36' AND el.event_id = e.id AND l.id = el.link_id";
  var sql="SELECT e.event,e.year,e.id,el.link_id,l.url  FROM events  e left Join  events_links el  on e.id=el.event_id left join links l on el.link_id=l.id WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = '36' ";
  console.log("SQLQUERY",sql) ;
  connection.query(sql, function(err, data , next) {
    if (err) throw err;
    else{
      //console.log("dBase====>",data);
      var prev = [];
////
      var x = {};
      var xc = {};
      var i = data.length;
      var dbDataLinks = [];
      while(i--) {
        var sHash = data[i].event.toString();
        var cHash = data[i].event.toString();
        if (typeof(x[sHash]) == "undefined") {
          x[sHash] = [];
          xc[cHash] = [];
        }
        if(xc[sHash].indexOf(data[i].url) == - 1 ){
          xc[cHash].push(data[i].url);
          x[sHash].push({
            'url': data[i].url,
            'id':data[i].id,
            'year':data[i].year  
          });
        } 
      }
      for(d in x){
        dbDataLinks.push({
          'id':x[d][0].id,
          'event':d,
          'url':x[d],
          'year':x[d][0].year
        })
      }
      var i =0
      for(f in xc){
        dbDataLinks[i].url = xc[f];
        i++;
      } 

      for(var k=0; k<eventsW.length; k++){  console.log("\n\n\n");
        var breaktag =0 ;
        var evtt = eventsW[k].event;
        var finalUrls = eventsW[k].eventUrl;
        //console.log("adasdfasdf==>",evtt,finalUrls);
        if(evtt && dbDataLinks.length>0){
          for(var i=0; i<dbDataLinks.length; i++){
            var evt = dbDataLinks[i].event;

            if(prev[0] == evt){
              //console.log('var difference = natural.LevenshteinDistance(prev[0],evt)');
            }else{
            if(eventsW[k].year == dbDataLinks[i].year){
              var lth = natural.LevenshteinDistance(evtt,evt);


              if(evtt.length>evt.length){
                var lth = ld(evt.trim(),evt.trim());
              }else{
                var lth = ld(evt.trim(),evtt.trim());
              }
              var dbEventLength  = evt.length;
              var lthPer = lthPercent(lth,dbEventLength);
              //console.log("db==>",evt,"wikievent==>",evtt,"score=-=>",lth,"len==>",evt.length,"percent==>",lthPer);
              if(lthPer <= 15){ 
                if (dbDataLinks[i].event.trim() == eventsW[k].event.trim()){
                  var dbUrlLen = dbDataLinks[i].url.length;
                  var wikiUrlLen = eventsW[k].eventUrl.length;
                  //console.log("LTH15call",dbUrlLen , wikiUrlLen);
                  for(var m = 0;m<wikiUrlLen;m++){
                        for (var l = 0;l< dbUrlLen;l++ ){
                          if( typeof  eventsW[k].eventUrl[m].url !='undefined' && typeof dbDataLinks[i].url[l] !='undefined'  && eventsW[k].eventUrl[m].url  && dbDataLinks[i].url[l] ){
                          //console.log(eventsW[k].eventUrl[m].url , dbDataLinks[i].url[l] );

                            if(eventsW[k].eventUrl[m].url.toLowerCase() == dbDataLinks[i].url[l].toLowerCase()){
                            if (dbUrlLen != wikiUrlLen ){
                              
                              var dif = Math.abs(wikiUrlLen- dbUrlLen);
                              //console.log("Method call",dif);
                              if (l == dbUrlLen-1 && dif > 0 )
                              { //console.log("Duplicate call");   
                                internalEvents.push({'id':dbDataLinks[i].id,'eventInternal':dbDataLinks[i].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
                                breaktag =1 ;
                                break;
                              }
                            }else{
                                 if(breaktag !=1){ 
                              //chk for links 
                               exitEvents.push({'id':dbDataLinks[i].id,'eventInternal':dbDataLinks[i].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl})
                               breaktag =1 ;
                               break;
                                   }

                            }

                        }  }else{
                               if(breaktag !=1){ 
                              //chk for links 
                               exitEvents.push({'id':dbDataLinks[i].id,'eventInternal':dbDataLinks[i].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl})
                               breaktag =1 ;
                               break;
                                   }
                            //console.log("here2","wiki==>",eventsW[k].eventUrl[m].url,"db==>",dbDataLinks[i].url[l]);
                          }


                       }
                        

                  }
                }else{
                /*exitEvents.push({'id':dbDataLinks[i].id,'eventInternal':dbDataLinks[i].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl})
                break;*/
                    if(breaktag !=1){ 
                              //chk for links 
                               exitEvents.push({'id':dbDataLinks[i].id,'eventInternal':dbDataLinks[i].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl})
                               breaktag =1 ;
                               break;
                                   }

              }
              }
              else if(lthPer > 15 && lthPer <= 50){
                //console.log('eventInternal2==>',dbDataLinks[i].event,'eventWiki2==>',eventsW[k].event)
                prev.length =0;
                prev.push(evt);
                internalEvents.push({'id':dbDataLinks[i].id,'eventInternal':dbDataLinks[i].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
                break;
              }else if(lthPer > 50 && lthPer <= 70){
                prev.length =0;
                prev.push(evt);
                newArray.push({'event':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
                break;
              }
              else if(evtt.length>80 && lthPer > 70){
                exitEvents.push({'id':dbDataLinks[i].id,'eventInternal':dbDataLinks[i].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
                break;
              }
            }
          }

          /***********/
          if(i == dbDataLinks.length-1){
                if(breaktag==0)
                {   
                      newArray.push({'event':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
                      break;
                }
            }
          /************/
          }
        }else{
  
          newArray.push({'event':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
        }

      }

    }
  res.json({internal:internalEvents,newevents:newArray,exitEvents:exitEvents});
  });
};



exports.birthItem = function(req , res){
  var births = req.body.wikiBirths;
  //console.log("births==>",births);
  var internalBirthEvents = [];
  var exitBirthEvents = [];
  var newBirthEvents = [];
  if(req.body.day && req.body.month){
    var day = req.body.day;
    var month = req.body.month;
  }
  else{
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
  }

  var sqll ="SELECT e.event,e.year,e.id,el.link_id,l.url  FROM events  e left Join  events_links el  on e.id=el.event_id left join links l on el.link_id=l.id WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = '37' ";
  connection.query(sqll, function(err, data) {
    if (err) throw err;
    else{
      //console.log("database==>",data);
      var prev = [];
      /******/
      var x = {};
      var xc = {};
      var i = data.length;
      var dbData = [];
      while(i--) {
        var sHash = data[i].event.toString();
        var cHash = data[i].event.toString();
        if (typeof(x[sHash]) == "undefined") {
          x[sHash] = [];
          xc[cHash] = [];
        }
        if(xc[sHash].indexOf(data[i].url) == - 1 ){
          xc[cHash].push(data[i].url);
          x[sHash].push({
            'url': data[i].url,
            'id':data[i].id,
            'year':data[i].year,
            'name': data[i].name
          });
        } 
      }
      for(d in x){
        dbData.push({
          'id':x[d][0].id,
          'event':d,
          'url':x[d],
          'year':x[d][0].year,
          'name':x[d][0].name
        })
      }
      var i =0
      for(f in xc){
        dbData[i].url = xc[f];
        i++;
      } 
      //console.log("dbdatalinks==>",dbData)

      /******/
      for(var k=0; k<births.length; k++){
        var breaktag =0 ;
        var evtt = births[k].event;
        var evttD = evtt.split('d.');
        if(evtt.search(',') == -1){
          var evttN = evtt.split(',');
          evntTxt = evtt;
        }else{
          var evttN = evtt.split(',');
          if(evttN[4]){
            evntTxt = evttN[1]+evttN[2]+evttN[3]+evttN[4];
          }else if(evttN[3]){
            evntTxt = evttN[1]+evttN[2]+evttN[3];
          }else if(evttN[2]){
            evntTxt = evttN[1]+evttN[2];
          }
          else{
            evntTxt = evttN[1];
          }
        }
        if(evtt && dbData.length>0){
          for(var i=0; i<dbData.length; i++){
            var evt = dbData[i].event;
            var evtD = evt.split('d.');
            var lth = natural.LevenshteinDistance(evntTxt.trim(),evt.trim());
           // evtt,evntTxt

            if(i == dbData.length-1 && births[k].year != dbData[i].year){
              newBirthEvents.push({'name':births[k].name,'event':evntTxt,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
              break;
            }
            if(births[k].year == dbData[i].year){
              if(evtt.length>evt.length){
                lth = ld(evt.trim(),evt.trim());
              }else{
                lth =ld(evt.trim(),evntTxt.trim());
              }
              var dbEventLength  = evt.length;
              var lthPer = lthPercent(lth,dbEventLength);
              //console.log("db==>",evt,"wikievent==>",evtt,"score=-=>",lth,"len==>",evt.length,"percent==>",lthPer);
              if(prev[0] == evt){
                //console.log('khj');
              }else{
                if(lthPer < 20){
                   if (dbData[i].event.trim() == evntTxt.trim()){
                     var dbUrlLen = dbData[i].url.length;
                     var wikiUrlLen = births[k].birthUrl.length;
                    //console.log("db==>",dbData[i].event,"wiki==>",evntTxt);
                    for(var m = 0;m<wikiUrlLen;m++){
                      for(var l = 0;l<dbUrlLen;l++){
                        if( typeof  births[k].birthUrl[m].url !='undefined' && typeof dbData[i].url[l] !='undefined'  && births[k].birthUrl[m].url  && dbData[i].url[l] ){
                        if(births[k].birthUrl[m].url.toLowerCase() == dbData[i].url[l].toLowerCase()){
                          //console.log("birth==>",births[k].event,"url==>",births[k].birthUrl[m].url,"db==>",dbData[i].event,"url==>",dbData[i].url[l]);
                          if (dbUrlLen != wikiUrlLen ){
                              var dif = Math.abs(wikiUrlLen- dbUrlLen);
                              if (l == dbUrlLen-1 && dif > 0 ){
                                internalBirthEvents.push({'name':births[k].name,'id':dbData[i].id,'eventInternal':evt,'eventWiki':evntTxt,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                                breaktag =1 ;
                                break;

                              }
                          }
                          else{
                              if(breaktag !=1){ 
                              //chk for links 
                                exitBirthEvents.push({'name':births[k].name,'id':dbData[i].id,'event':evntTxt,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                                breaktag =1 ;
                                break;
                              }
                          }




                        }
                      }
                      else{
                               if(breaktag !=1){ 
                              //chk for links 
                               exitBirthEvents.push({'name':births[k].name,'id':dbData[i].id,'event':evntTxt,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                               breaktag =1 ;
                               break;
                                   }
                            //console.log("here2","wiki==>",eventsW[k].eventUrl[m].url,"db==>",dbDataLinks[i].url[l]);
                          }

                      }



                    }
                  }else{
                  // exitBirthEvents.push({'name':births[k].name,'id':dbData[i].id,'event':evntTxt,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                  // break;
                    if(breaktag !=1){ 
                      //chk for links 
                      exitBirthEvents.push({'name':births[k].name,'id':dbData[i].id,'event':evntTxt,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                      breaktag =1 ;
                      break;
                    }
                }
              }
                if(lthPer >20 && lthPer <= 40){
                  if(evttD[1] == evtD[1]){
                    prev.length =0;
                    prev.push(evt);
                    internalBirthEvents.push({'name':births[k].name,'id':dbData[i].id,'eventInternal':evt,'eventWiki':evntTxt,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                    break;
                  }
                }else if(lthPer > 40){
                  prev.length =0;
                  prev.push(evt);
                  newBirthEvents.push({'name':births[k].name,'event':evntTxt,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                  break;
                }
              }
            }
          }}else{
            newBirthEvents.push({'name':births[k].name,'event':evntTxt,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
          }
      }
    }
    //console.log("exitBirthEvents===>",exitBirthEvents);
  res.json({ exitBirthEvents:exitBirthEvents, internalBirths:internalBirthEvents , NewBirths:newBirthEvents});
  });
};



exports.deathItem = function(req , res){
  var deaths = req.body.wikiDeaths;
  //console.log("sdxsa",deaths);
  var exitDeathEvents = [];
  var internalDeathEvents = [];
  var newDeathEvents = [];
  if(req.body.day && req.body.month){
    var day = req.body.day;
    var month = req.body.month;
  }
  else{
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
  }
  var sq = "SELECT e.event,e.name,e.year,e.id,el.link_id,l.url FROM events e,links l,events_links el WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = '38' AND el.event_id = e.id AND l.id = el.link_id";
  connection.query(sq, function(err, data) {
    if (err) throw err;
    else{
      //console.log("data==>",data);
      var prev = [];
      /******/
      ////
      var x = {};
      var xc = {};
      var i = data.length;
      var dbData = [];
      while(i--) {
        var sHash = data[i].event.toString();
        var cHash = data[i].event.toString();
        if (typeof(x[sHash]) == "undefined") {
          x[sHash] = [];
          xc[cHash] = [];
        }
        if(xc[sHash].indexOf(data[i].url) == - 1 ){
          xc[cHash].push(data[i].url);
          x[sHash].push({
            'url': data[i].url,
            'id':data[i].id,
            'year':data[i].year,
            'name': data[i].name
          });
        } 
      }
      for(d in x){
        dbData.push({
          'id':x[d][0].id,
          'event':d,
          'url':x[d],
          'year':x[d][0].year,
          'name':x[d][0].name
        })
      }
      var i =0
      for(f in xc){
        dbData[i].url = xc[f];
        i++;
      } 
      //console.log("dbdatalinks==>",dbDataLinks)

      /******/

      for(var k=0; k<deaths.length; k++){
        var evtt = deaths[k].event;
        //console.log("scffs",evtt);
        var evttB = evtt.split('b.');
        if(evtt.search(',') == -1){
          var evttN = evtt.split(',');
          evntTxt = evtt;
        }else{
          var evttN = evtt.split(',');
          if(evttN[4]){
            evntTxt = evttN[1]+evttN[2]+evttN[3]+evttN[4];
          }else if(evttN[3]){
            evntTxt = evttN[1]+evttN[2]+evttN[3];
          }else if(evttN[2]){
            evntTxt = evttN[1]+evttN[2];
          }
          else{
            evntTxt = evttN[1];
          }
        }
        if(evtt && dbData.length>0){
          for(var i=0; i<dbData.length; i++){
            var evt = dbData[i].event;
            var evtB = evt.split('b.');
            if(evtt.length>evt.length){
              var lth = ld(evt.trim(),evt.trim());
              var  dataC = evtt;
            }else{
              var lth = ld(evt.trim(),evntTxt.trim());
              var dataC = evt;
            }
            if(i == dbData.length-1 && deaths[k].year != dbData[i].year){
              newDeathEvents.push({'name':deaths[k].name,'event':evntTxt,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
              break;
            }
            if(prev[0] == evt){
                //console.log('khj');
            }else{
            if(deaths[k].year == dbData[i].year){
              if(lth > 20 && lth <=40){
                if(evttB[1] == evtB[1]){
                  prev.length = 0;
                  prev.push(evtt);
                  internalDeathEvents.push({'name':deaths[k].name,'id':dbData[i].id,'eventInternal':dbData[i].event,'eventWiki':evntTxt,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                  break;
                }else{
                  if(dbData[i].name.length>evttN[0].length)
                    {
                      lth1 = ld(dbData[i].name,evttN[0]);
                    }else{
                      lth1 = ld(evttN[0],dbData[i].name);
                    }
                  if(lth1>5 && lth1<=10){
                  prev.length = 0;
                  prev.push(evtt);
                  internalDeathEvents.push({'name':deaths[k].name,'id':dbData[i].id,'eventInternal':dbData[i].event,'eventWiki':evntTxt,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                  break;}

                  if(lth1 <=5){
                    exitDeathEvents.push({'name':deaths[k].name,'event':evntTxt,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                    break;
                  }
                }
              }else if(lth > dataC.length/1.4){
                prev.length = 0;
                prev.push(evtt);
                newDeathEvents.push({'name':deaths[k].name,'event':evntTxt,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                break;
              }
              else if(lth <= 20){
                if(dbData[i].event.trim() == evntTxt.trim()){
                  //console.log("1==>",dbData[i].event ,"url=>",dbData[i].url, "2==>",evntTxt,"url=>",deaths[k].birthUrl);
                  var dbUrlLen = dbData[i].url.length;
                  var wikiUrlLen = deaths[k].birthUrl.length;
                  //console.log("1==>",dbUrlLen ,"url=>",dbData[i].url, "2==>",wikiUrlLen,"url=>",deaths[k].birthUrl);
                  for(var m = 0;m<wikiUrlLen;m++){  
                    for (var l = 0;l< dbUrlLen;l++ ){
                        if(deaths[k].birthUrl[m].url.toLowerCase() == dbData[i].url[l].toLowerCase()){
                          if (dbUrlLen != wikiUrlLen ){
                              var dif = wikiUrlLen- dbUrlLen;
                              if (l == dif)
                              { 
                                internalDeathEvents.push({'name':deaths[k].name,'id':dbData[i].id,'eventInternal':dbData[i].event,'eventWiki':evntTxt,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                                breaktag =1 ;
                                break;
                              }
                            }
                        }
                    }
                  }




                }else{
                exitDeathEvents.push({'name':deaths[k].name,'id':dbData[i].id,'event':evntTxt,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                break;
              }
              }
            }}
          }
        }else{
          newDeathEvents.push({'name':deaths[k].name,'event':evntTxt,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
        }
      }
    }
    //console.log("internalDeathEvents",internalDeathEvents);
  res.json({ exitDeathEvent: exitDeathEvents,internalDeaths: internalDeathEvents , NewDeaths:newDeathEvents});
  });
};
// 
/*exports.holidayItems = function(req , res){
  var holidays = req.body.wikiHolidays;
  //console.log("holidays",holidays);
  var exitHolidayEvents = [];
  var internalHolidayEvents = [];
  var newHolidayEvents = [];
  if(req.body.day && req.body.month){
    var day = req.body.day;
    var month = req.body.month;
  }
  else{
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
  }
  var sq = "SELECT * FROM events  WHERE day = '"+ day +"' AND month = '"+ month +"' AND category_id = '39'" ;
  connection.query(sq, function(err, data) {
    if (err) throw err;
    else{
      for(var k=0; k<holidays.length-1; k++){
        var evtt = holidays[k].event;
        if(evtt && data.length>0){
          for(var i=0; i<data.length-1; i++){
            var evt = data[i].event;
          
            if(evt.length > evtt.length){
              var lth = ld(evt,evt);
            }else{
              var lth = ld(evtt,evt);
            }
            //console.log("insidefor","wikievnt",evtt,"dbevent",evt,"lth",lth);
            if(lth<=10){
              exitHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
               break;
            }
            else if(lth>10 && lth <= 30){
              internalHolidayEvents.push({'event':holidays[k].event,'id':data[i].id,'finalUrls':holidays[k].birthUrl});
               break;
            }else if(lth > 30){
              newHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                break;
            }
            //break;
          }
        }else{
          newHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
        }
      }
    }
    //console.log("internalholidayevent",internalHolidayEvents);
    res.json({ exitHolidayEvents:exitHolidayEvents, NewHolidays:newHolidayEvents , internalHolidays:internalHolidayEvents});
  });
};*/



//new code to get holiday items SACHIN///////

exports.holidayItem = function(req , res){
  var holidays = req.body.wikiHolidays;
  //console.log("holidays",holidays);
  var exitHolidayEvents = [];
  var internalHolidayEvents = [];
  var newHolidayEvents = [];
  if(req.body.day && req.body.month){
    var day = req.body.day;
    var month = req.body.month;
  }
  else{
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
  }
  var idarray = [];
  //var sq = "SELECT e.event,e.name,e.year,e.id,el.link_id,l.url FROM events e,links l,events_links el WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = '39' AND el.event_id = e.id AND l.id = el.link_id";
  var sq="SELECT l.url,el.link_id ,e.event,e.id  FROM events  e left Join  events_links el  on e.id=el.event_id left join links l on el.link_id=l.id WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = 39 ";
  connection.query(sq, function(err, data) {
    if (err) throw err;
    else{
      //console.log("data==>",data);

      var prev = [];
      /******/
      var x = {};
      var xc = {};
      var i = data.length;
      var dbData = [];
      while(i--) {
        var sHash = data[i].event.toString();
        var cHash = data[i].event.toString();
        if (typeof(x[sHash]) == "undefined") {
          x[sHash] = [];
          xc[cHash] = [];
        }
        if(xc[sHash].indexOf(data[i].url) == - 1 ){
          xc[cHash].push(data[i].url);
          x[sHash].push({
            'url': data[i].url,
            'id':data[i].id,
            //'year':data[i].year,
            //'name': data[i].name
          });
        } 
      }
      for(d in x){
        dbData.push({
          'id':x[d][0].id,
          'event':d,
          'url':x[d],
         // 'year':x[d][0].year,
         // 'name':x[d][0].name
        })
      }
      var i =0
      for(f in xc){
        dbData[i].url = xc[f];
        i++;
      } 
      //console.log("dbdatalinks==>",dbData)

      /******/
      for(var k =1; k < holidays.length; k++ ){
        var breaktag =0 ;
        var wikievnt = holidays[k].event;
        if(wikievnt && dbData.length>0){
          for(var i = 0;i<dbData.length;i++){
            var dbevnt = dbData[i].event;
            //console.log("dbevnt==>",dbevnt);
            if(dbevnt.length > wikievnt.length){
              var lth = ld(dbevnt.trim(),wikievnt.trim());
            }else{ 
              var lth = ld(wikievnt.trim(),dbevnt.trim());  
                       
            }
          
            if(lth<=10){
              if(dbData[i].event.trim() == evntTxt.trim()){
                var dbUrlLen = dbData[i].url.length;
                var wikiUrlLen = holidays[k].birthUrl.length;
                for(var m = 0;m<wikiUrlLen;m++){
                      for(var l = 0;l<dbUrlLen;l++){
                        if(holidays[k].birthUrl[m].url.toLowerCase() == dbData[i].url[l].toLowerCase()){
                          //console.log("birth==>",births[k].event,"url==>",births[k].birthUrl[m].url,"db==>",dbData[i].event,"url==>",dbData[i].url[l]);
                          if (dbUrlLen != wikiUrlLen ){
                              var dif = wikiUrlLen- dbUrlLen;
                              if (l == dif){
                                internalHolidayEvents.push({'event':dbData[i].event,'id':dbData[i].id,'finalUrls':holidays[k].birthUrl});
                                breaktag =1 ;
                                break;

                              }
                          }
                        }
                      }
                }
              }
              if(idarray.indexOf(holidays[k].event) == -1){
                idarray.push(holidays[k].event);
                exitHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                //break;
              }
            } 
            else if(lth>10 && lth <= 30){
              //console.log("wiki",holidays[k].event,"db",data[i].event);
              if(idarray.indexOf(dbData[i].event) == -1){
                idarray.push(dbData[i].event);
                internalHolidayEvents.push({'event':dbData[i].event,'id':dbData[i].id,'finalUrls':holidays[k].birthUrl});
                //break;
              }
            }
            else if(lth > 30){
              if(idarray.indexOf(holidays[k].event) == -1){
                idarray.push(holidays[k].event);
                //console.log("RRRR",idarray);            
                newHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                //break;
             } 
          
            }
          }   
        }else{    

          if(idarray.indexOf(holidays[k].event) == -1){
              idarray.push(holidays[k].event);
              newHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
          }

        }
      }
    }
    //console.log("internalholidayevent",internalHolidayEvents);
    res.json({ exitHolidayEvents:exitHolidayEvents, NewHolidays:newHolidayEvents , internalHolidays:internalHolidayEvents});
  });
};


//Save duplicate events SACHIN

exports.savedupEvents = function(req , res){
  //console.log("data",req.body);
  for (var j = 0; j<req.body.length;j++){
    var evt = req.body[j].event;
    var eventid = req.body[j].eventid;
    var evnt = evt.replace(/'/g, "''");

    var sq = "UPDATE events SET event ='"+evnt+"' WHERE id ='"+eventid+"'";
    abc(sq, function(data){
      console.log("Successfully updated");
    });
    if(req.body.length - 1 == j){
      res.json({"message":"DONE"});
    }

  }
  function abc(qur, callback){
    connection.query(qur, function(err, data) {
      if (err) 
        throw err;
      else{
        // console.log("Successfully updated");
      }
      callback('OK'); 
    });
  }


}

//Delete the existing events indivisually
exports.deleteExistEvents = function(req , res){
  console.log("data===>",req.body);
  var event_id = req.body.eventid;
  var category_id = req.body.category;
  var dbevent = req.body.event;
  if (typeof event_id == 'undefined'){
    var sq = "DELETE FROM events  WHERE event  = '"+ dbevent +"' AND category_id = '"+ category_id +"'" ;
  }else{
    var sq = "DELETE FROM events  WHERE id  = '"+ event_id +"' AND category_id = '"+ category_id +"'" ;
  }
  connection.query(sq, function(err, data) {
    if (err) throw err;
    else{
      res.json({"message":"DONE"});
    }
  });
}



function lthPercent  (score, length) {
  var percent = (score/length)*100;
  return percent;
}